resolvers += "Sonatype OSS Snapshots" at "https://oss.sonatype.org/content/repositories/snapshots"

addSbtPlugin("info.schleichardt" % "sbt-sonar" % "0.2.0-SNAPSHOT")

addSbtPlugin("com.github.gseitz" % "sbt-release" % "0.8.5")
