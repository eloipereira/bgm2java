package edu.berkeley.eloi.bigraph;

import java.util.Arrays;

/**
 * Created by eloi on 8/13/14.
 */
public class ExampleMatching {
    public static void main(String[] args){

        Bigraph b = new Bigraph("l1_Location.(u0_UAV[network]|u1_UAV[network]) | l0_Location;");
        Bigraph redex = new Bigraph("l0_Location.(u1_UAV[network] | $0);");
        BigMCAPI.matching(b,redex);
    }

}
