package edu.berkeley.eloi.bigraph;

import edu.berkeley.eloi.concreteBgm2Java.ConcreteBgm2JavaCompiler;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class Bigraph implements Serializable {
    private PlaceList places;
    private static final Log LOG = LogFactory.getLog(Bigraph.class);

    public Bigraph() {
        this.places = new PlaceList();
    }

    public Bigraph(PlaceList places) {
        this.places = places;
    }

    public Bigraph(String term){
        this.places = ConcreteBgm2JavaCompiler.generate(term, false).bigraph.places;
    }

    public PlaceList getPlaces() {
        return this.places;
    }

    public List<Region> getRegions() {
        return places.getRegions();
    }

    public List<BigraphNode> getNodes() {
        return places.getNodes();
    }

    public List<Hole> getHoles() {
        return places.getHoles();
    }

    public List<BigraphNode> getNodesWithControlID(String ctrId) {
        List<BigraphNode> bigraphNodes = this.getNodes();
        List<BigraphNode> nodesCtr = new ArrayList<BigraphNode>();
        for (BigraphNode n : bigraphNodes) {
            if (n.getCtrId().equals(ctrId)) {
                nodesCtr.add(n);
            }
        }
        return nodesCtr;
    }

    public BigraphNode getNode(String nodeId) {
        List<String> ids = new ArrayList<String>();
        for (BigraphNode n : places.getNodes()) {
            ids.add(n.getId());
        }
        int index = ids.indexOf(nodeId);
        if (index == -1) {
            System.err.println("BigraphNode " + nodeId + " does not exists");
            return null;
        }
        return places.getNodes().get(index);
    }

    public Place getParentOf(String nodeId) {
        return this.getNode(nodeId).getParent();
    }

    public List<Place> getChildrenOf(Place place) {
        List<Place> children = new ArrayList<Place>();
        for (Place n : places) {
            if (place.equals(n.getParent())) {
                children.add(n);
            }
        }
        return children;
    }

    public List<BigraphNode> getLinkedTo(String nodeId) {
        BigraphNode node = this.getNode(nodeId);
        List<BigraphNode> linked = new ArrayList<BigraphNode>();
        List<String> sharedNames = new ArrayList<String>();
        for (BigraphNode n : places.getNodes()) {
            sharedNames.addAll(n.getNames());
            sharedNames.retainAll(node.getNames());
            if (!sharedNames.isEmpty() & n != node) {
                linked.add(n);
            }
            sharedNames.clear();
        }
        return linked;
    }

    protected void incrementRegionsId(int val) {
        for (Place p : this.places) {
            if (p instanceof Region) {
                ((Region) p).incrementId(val);
            } else if (p.getParent() instanceof Region) {
                Region region = (Region) p.getParent();
                region.incrementId(val);
                p.setParent(region);
            }
        }
    }

    protected int getRegionsMaxVal() {
        int maxVal = 0;
        for (Place p : this.places) {
            if (p instanceof Region && ((Region) p).getId() > maxVal) {
                    maxVal = ((Region) p).getId();
            }
        }
        return maxVal;
    }

    public void parallel(Bigraph bigraph) {
        bigraph.incrementRegionsId(this.getRegionsMaxVal() + 1);
        boolean isDisjoint = Collections.disjoint(this.places, bigraph.places);
        if (isDisjoint) {
            this.places.addAll(bigraph.places);
        } else {
            System.err.println("Bigraph supports are not disjoint");
        }

    }

    public String getTerm() {
        return getRegionsTerm(places.getRegions());
    }

    private String getRegionsTerm(List<Region> regions) {
        StringBuffer buf = new StringBuffer();
        for (int i = 0; i < regions.size(); i++) {
            if (i > 0) {
                buf.append("||");
            }
            buf.append(getTermChildren(this.getChildrenOf(regions.get(i))));
        }
        return buf.toString();
    }

    private String getTermChildren(List<Place> nodes) {
        StringBuilder term = new StringBuilder();
        for (int i = 0; i < nodes.size(); i++) {
            if (i > 0) {
                term.append("|");
            }
            term.append(nodes.get(i).toBgm());
            if (!this.getChildrenOf(nodes.get(i)).isEmpty()) {
                term.append(".(" + getTermChildren(this.getChildrenOf(nodes.get(i))) + ")");
            }
        }
        return term.toString();
    }


    public Boolean matching(Bigraph redex){
        return BigMCAPI.matching(this,redex);
    }


    @Override
    public String toString() {
        return getTerm();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o){
            return true;
        }
        if (!(o instanceof Bigraph)){
            return false;
        }

        Bigraph bigraph = (Bigraph) o;

        if (places.containsAll(bigraph.getPlaces()) && bigraph.getPlaces().containsAll(places)) {
            return true;
        }

        if (this.matching(bigraph)){
            return true;
        }

        if (bigraph.matching(this)) {
            return true;
        }

        return false;
    }

    @Override
    public int hashCode() {
        return places != null ? places.hashCode() : 0;
    }
}
