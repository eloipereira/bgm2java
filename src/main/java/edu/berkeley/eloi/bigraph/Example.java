package edu.berkeley.eloi.bigraph;

import java.util.Arrays;

/**
 * Created by eloi on 8/13/14.
 */
public class Example {
    public static void main(String[] args){

        Bigraph b = new Bigraph("l1_Location.(u0_UAV[network]|u1_UAV[network]) | l0_Location;");
        BRS brs = new BRS(b,true);
        BRR brr = new BRR("l1_Location.(u1_UAV[network] | $0) | l0_Location.$1 -> l1_Location.$0 | l0_Location.(u1_UAV[network] | $1)");
        brs.applyRules(Arrays.asList(brr));

    }

}
