package edu.berkeley.eloi.bigraph;

import java.io.Serializable;

public class Control implements Serializable {
    private String id;
    private int arity;
    private Activity activity;

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Control)) {
            return false;
        }

        Control control = (Control) o;

        if (arity != control.arity) {
            return false;
        }
        if (activity != null ? !activity.equals(control.activity) : control.activity != null) {
            return false;
        }
        if (id != null ? !id.equals(control.id) : control.id != null) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + arity;
        result = 31 * result + (activity != null ? activity.hashCode() : 0);
        return result;
    }

    public Control(String id, int arity, Activity activity) {
        this.id = id;
        this.arity = arity;
        this.activity = activity;
    }

    static abstract class Activity {
    }

    public static class Active extends Activity {
        @Override
        public String toString() {
            return "%active";
        }
    }

    public static class Passive extends Activity {
        @Override
        public String toString() {
            return "%passive";
        }
    }

    public static class Atomic extends Activity {
        @Override
        public String toString() {
            return "%atomic";
        }
    }

    public String getId() {
        return id;
    }

    public int getArity() {
        return arity;
    }

    public Activity getActivity() {
        return activity;
    }

    @Override
    public String toString() {
        return activity + " " + id + ':' + arity;
    }
}
