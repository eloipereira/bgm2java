package edu.berkeley.eloi.bigraph;

import edu.berkeley.eloi.concreteBgm2Java.ConcreteBgmTreeWalker;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static edu.berkeley.eloi.concreteBgm2Java.ConcreteBgm2JavaCompiler.generate;

/**
 * Created by eloi on 7/30/14.
 */
public final class BigMCAPI {
    private BigMCAPI(){};

    private static final Log LOG = LogFactory.getLog(BigMCAPI.class);

    private static final Pattern p0 = Pattern.compile("(\\d+):\\s+(.*)");
    private static final Pattern p1 = Pattern.compile("#(\\d+)\\s+(.*)\\s+<-\\s+\\*{3}\\s+VIOLATION\\s+\\*{3}");


    private static String generateBigMCProgram(Bigraph bigraph, List<BRR> brrs, List<Property> ps){
        List<Control> signature = new ArrayList<>();
        List<String> names = new ArrayList<>();

        StringBuffer buf = new StringBuffer();
        List<BigraphNode> allNodes = bigraph.getNodes();

        for (BRR brr: brrs) {
            allNodes.addAll(brr.getRedex().getNodes());
            allNodes.addAll(brr.getReactum().getNodes());
        }

        for (BigraphNode n : allNodes) {
            Control ctr = new Control(n.getId() + "_" + n.getCtrId(), n.getNames().size(), new Control.Active());
            if (!signature.contains(ctr)) {
                signature.add(ctr);
            }
            List<String> nodeNames = n.getNames();
            for (String m : nodeNames) {
                if (!names.contains(m)) {
                    names.add(m);
                }
            }
        }
        for (Control c : signature) {
            buf.append(c.getActivity().toString() + " " + c.getId() + ":" + c.getArity() + ";\n");
        }
        for (String n : names) {
            buf.append("%name " + n + ";\n");
        }
        for (BRR brr: brrs) {
            buf.append(brr.getRedex() + " -> " + brr.getReactum() + ";\n"); //TODO - I removed "%rule id" because it is not running in the current version o BigMC. Still I kept it in the parser since BigRED generates the rules with that annotation. I should probably remove it to avoid confusion
        }
        buf.append(bigraph.getTerm() + ";\n");
        for (Property p: ps){
            buf.append("%property p" + p.hashCode() + " " + p.toString() + ";\n");
        }

        buf.append("%check");
        return buf.toString();
    }

    public static BigMCResult runBigMC(Bigraph bigraph, List<BRR> brrs, List<Property> ps,int steps){
        String code = generateBigMCProgram(bigraph,brrs,ps);
        final File tmp;
        try {
            tmp = File.createTempFile("bigmc_model", ".bgm");
            saveFile(tmp, code);
            String cmdLine = "bigmc" + " -m " + steps + " -p " + tmp.getAbsolutePath();
            final String cl = cmdLine;
            final Process process = Runtime.getRuntime().exec(cl);
            InputStream os = process.getInputStream();
            InputStreamReader reader = new InputStreamReader(os);
            final BufferedReader br = new BufferedReader(reader);
            String c;
            List<String> terms = new ArrayList<>();
            List<String> counters = new ArrayList<>();

            while ((c = br.readLine()) != null) {
                Matcher m0 = p0.matcher(c);
                Matcher m1 = p1.matcher(c);
                if (m0.matches()){
                    String term = m0.group(2).replaceAll("\\.nil", ""); // TODO - Parser does not deal well with ".nil". Must fix!
                    LOG.debug("Bigraph term: " + term);
                    terms.add(term);
                } else if (m1.matches()){
                    String counter = m1.group(2).replaceAll("\\.nil", ""); // TODO - Parser does not deal well with ".nil". Must fix!
                    LOG.debug("Counter example: " + counter);
                    counters.add(counter);
                }
            }
            try {
                process.waitFor();
            } catch (InterruptedException e) {
                LOG.error(e);
            }
            os.close();
            reader.close();
            br.close();
            boolean delete = tmp.delete();
            if(!delete){
                LOG.error("Temporary file failed to delete.");
            }
            return new BigMCResult(terms,counters,!counters.isEmpty());
        } catch (IOException e) {
            LOG.error(e);
            return null;
        }
    }


    public static Boolean matching(Bigraph bigraph, Bigraph pattern){
        Property match = new MATCH(pattern.getTerm());
        List<BRR> brrs = new ArrayList<BRR>();
        List<Property> props = new ArrayList<>();
        props.add(match);
        BigMCResult result =runBigMC(bigraph, brrs, props, 2);
        LOG.debug("Matching result: " + result);
        return !result.violation;
    }

    public static BigraphApplicationResult getNextBigraph(Bigraph bigraph, BRR brr){
        List<BRR> brrs = new ArrayList<>();
        brrs.add(brr);
        boolean gotNewTerm = false;
        List<String> terms = runBigMC(bigraph,brrs,new ArrayList<Property>(),2).terms;
        if (terms.size() > 1){
            gotNewTerm = true;
        }
        Bigraph result;
        if (gotNewTerm) {
            String term = terms.get(1);
            LOG.debug("Bigraph Result: " + term);
            ConcreteBgmTreeWalker.program_return gen =  generate(term + ";\n", false);
            result = gen.bigraph;
        } else {
            result = bigraph;
        }
        return new BigraphApplicationResult(result,gotNewTerm);
    }

    public static class BigMCResult{
        private List<String> terms;
        private List<String> counterExamples;
        private Boolean violation;

        public BigMCResult(List<String> terms, List<String> counterExamples, Boolean violation) {
            this.terms = terms;
            this.counterExamples = counterExamples;
            this.violation = violation;
        }

        @Override
        public String toString() {
            return "BigMCResult{" +
                    "terms=" + terms +
                    ", counterExamples=" + counterExamples +
                    ", violation=" + violation +
                    '}';
        }
    }


    public static class BigraphApplicationResult{
        private Bigraph bigraph;
        private Boolean isNew;

        public Bigraph getBigraph() {
            return bigraph;
        }

        public Boolean getIsNew() {
            return isNew;
        }

        private BigraphApplicationResult(Bigraph b, Boolean isNew) {
            this.bigraph = b;
            this.isNew = isNew;
        }
    }

    private static void saveFile(File fp, String code) {
        try {
            BufferedWriter w = new BufferedWriter(new FileWriter(fp));
            w.write(code);
            w.close();
        } catch (IOException e) {
            LOG.error(e);
        }
    }

    public static abstract class Property{};
    public static class MATCH extends Property{
        String term;
        public MATCH(String term) {
            this.term = term;
        }
        public MATCH(Bigraph b) {
            this.term = b.getTerm();
        }
        @Override
        public String toString() {
            return "matches(" + term + ")";
        }
    }
    public static class TERMINAL extends Property{
        @Override
        public String toString() {
            return "terminal()";
        }
    }
    public static class AND extends Property{
        Property p0;
        Property p1;
        public AND(Property p0, Property p1){
            this.p0=p0;
            this.p1=p1;
        }
        @Override
        public String toString(){
            return "(" + p0.toString() + "&&" + p1.toString()+ ")" ;
        }
    }
    public static class OR extends Property{
        Property p0;
        Property p1;
        public OR(Property p0, Property p1){
            this.p0=p0;
            this.p1=p1;
        }
        @Override
        public String toString(){
            return "(" + p0.toString() + "||" + p1.toString() + ")";
        }
    }
    public static class NOT extends Property{
        Property p;
        public NOT(Property p){
            this.p=p;
        }
        @Override
        public String toString(){
            return "!(" + p.toString() + ")";
        }
    }

}
