package edu.berkeley.eloi.bigraph;

import edu.berkeley.eloi.bigvis.Visualizer;
import edu.berkeley.eloi.concreteBgm2Java.ConcreteBgmTreeWalker;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

import static edu.berkeley.eloi.concreteBgm2Java.ConcreteBgm2JavaCompiler.generate;

public class BRS extends Visualizer implements Serializable {
    private List<Control> signature;
    private List<String> names;
    private Bigraph bigraph;
    private List<BRR> rules;

    private static final Log LOG = LogFactory.getLog(BRS.class);

    public BRS(List<Control> signature, List<String> names, Bigraph bigraph, List<BRR> rules, boolean visualize) {
        super("");
        this.signature = signature;
        this.names = names;
        this.bigraph = bigraph;
        this.rules = rules;
        this.update(this.bigraph.getTerm());
        this.setVisible(visualize);
    }

    public BRS(Bigraph bigraph) {
        super("");
        this.bigraph = bigraph;
        this.setVisible(false);
        this.signature = new ArrayList<Control>();
        this.names = new ArrayList<String>();
        this.rules = new ArrayList<BRR>();
    }

    public BRS(Bigraph bigraph, boolean visualize) {
        super("");
        this.bigraph = bigraph;
        this.setVisible(visualize);
        this.signature = new ArrayList<Control>();
        this.names = new ArrayList<String>();
        this.rules = new ArrayList<BRR>();
    }

    public BRS(String bgmFilePath){
        super("");
        ConcreteBgmTreeWalker.program_return gen = generate(bgmFilePath, true);
        this.signature = gen.signature;
        this.names = gen.names;
        this.bigraph = gen.bigraph;
        this.rules = gen.rules;
        this.update(this.bigraph.getTerm());
        this.setVisible(false);
    }

    public BRS(String bgmFilePath, boolean visualize){
        super("");
            ConcreteBgmTreeWalker.program_return gen = generate(bgmFilePath, true);
            this.signature = gen.signature;
            this.names = gen.names;
            this.bigraph = gen.bigraph;
            this.rules = gen.rules;
            this.update(this.bigraph.getTerm());
            this.setVisible(visualize);
    }

    public List<Control> getSignature() {
        return signature;
    }

    public List<String> getNames() {
        return names;
    }

    public Bigraph getBigraph() {
        return bigraph;
    }

    public List<BRR> getRules() {
        return rules;
    }


    @Override
    public String toString() {
        return bigraph.toString();
    }


    public Boolean applyRules(List<BRR> rules){
        Bigraph currentBigraph = this.bigraph;
        BigMCAPI.BigraphApplicationResult result;
        Boolean isNew = false;
        for (BRR r: rules){
            result = BigMCAPI.getNextBigraph(currentBigraph,r);
            currentBigraph = result.getBigraph();
            isNew = isNew || result.getIsNew();
        }
        this.bigraph = currentBigraph;
        if (isNew) {
            LOG.debug("New Bigraph: " + this.bigraph.getTerm());
            this.update(this.bigraph.getTerm());
        } else{
            LOG.warn("BRR(s) attempted to change the bigraph but the bigraph did not change.");
            this.update(this.bigraph.getTerm());
        }
        return isNew;
    }

}
