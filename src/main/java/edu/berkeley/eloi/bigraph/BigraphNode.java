package edu.berkeley.eloi.bigraph;

import java.util.List;

public class BigraphNode extends AbstractPlace<String> {
    private String ctrId;
    private List<String> names;

    public BigraphNode(String id) {
        super(id);
    }

    public BigraphNode(String id, String ctrId, List<String> names, Place parent) {
        super(id);
        this.ctrId = ctrId;
        this.names = names;
        this.parent = parent;
    }

    public String getCtrId() {
        return ctrId;
    }

    public List<String> getNames() {
        return names;
    }

    public void setNames(List<String> names) {this.names = names;}

    public String toBgm() {
        String term = this.getId() + "_" + this.getCtrId();
        if (!this.getNames().isEmpty()) {
            term += this.getNames().toString();
        }
        return term;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof BigraphNode)){
            return false;
        }
        if (!super.equals(o)){
            return false;
        }

        BigraphNode that = (BigraphNode) o;

        if (!ctrId.equals(that.ctrId)){
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + ctrId.hashCode();
        return result;
    }
}
