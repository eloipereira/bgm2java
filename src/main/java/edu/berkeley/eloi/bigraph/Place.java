package edu.berkeley.eloi.bigraph;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: eloipereira
 * Date: 5/17/13
 * Time: 1:47 PM
 * To change this template use File | Settings | File Templates.
 */
public interface Place {
    Object getId();

    Place getParent();
    void setParent(Place parent);

    List<String> getNames();

    void setNames(List<String> names);

    String toBgm();
}
