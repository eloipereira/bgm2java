package edu.berkeley.eloi.bigraph;

import java.io.Serializable;

/**
 * Created with IntelliJ IDEA.
 * User: eloipereira
 * Date: 5/16/13
 * Time: 11:29 AM
 * To change this template use File | Settings | File Templates.
 */
public abstract class AbstractPlace<T> implements Place, Serializable {
    protected T id;
    protected Place parent;

    public AbstractPlace(T id) {
        this.id = id;
    }

    public T getId() {
        return id;
    }

    public Place getParent() {
        return parent;
    }

    public void setParent(Place parent) {
        this.parent = parent;
    }

    @Override
    public String toString() {
        return id.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o){
            return true;
        }
        if (!(o instanceof AbstractPlace)){
            return false;
        }

        AbstractPlace that = (AbstractPlace) o;

        if (!id.equals(that.id)) {
            return false;
        }
        if (parent != null ? !parent.equals(that.parent) : that.parent != null){
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + (parent != null ? parent.hashCode() : 0);
        return result;
    }
}
