package edu.berkeley.eloi.bigraph;

import java.util.List;

public class Region extends AbstractPlace<Integer> {

    public Region(Integer id) {
        super(id);
        super.parent = null;
    }

    protected void incrementId(int val) {
        this.id = this.id + val;
    }

    @Override
    public List<String> getNames() {
        return null;
    }

    @Override
    public void setNames(List<String> names) {

    }

    public String toBgm() {
        return "";
    }


}
