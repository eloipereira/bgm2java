package edu.berkeley.eloi.bigraph;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: eloipereira
 * Date: 5/16/13
 * Time: 1:40 PM
 * To change this template use File | Settings | File Templates.
 */
public class PlaceList extends ArrayList<Place> {

    public PlaceList(int i) {
        super(i);
    }

    public PlaceList() {
    }

    public PlaceList(Collection<? extends Place> abstractNodes) {
        super(abstractNodes);
    }

    List<BigraphNode> getNodes() {
        return getListOfType(BigraphNode.class);
    }

    List<Region> getRegions() {
        return getListOfType(Region.class);
    }

    List<Hole> getHoles() {
        return getListOfType(Hole.class);
    }

    private <T extends AbstractPlace> List<T> getListOfType(Class C) {
        List<T> nodes = new ArrayList<T>();
        for (int i = 0; i < this.size(); i++) {
            T n = (T) this.get(i);
            if (n.getClass().equals(C)) {
                nodes.add(n);
            }
        }
        return nodes;
    }
}
