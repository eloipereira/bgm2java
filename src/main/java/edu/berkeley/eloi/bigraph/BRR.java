package edu.berkeley.eloi.bigraph;

import edu.berkeley.eloi.concreteBgm2Java.ConcreteBgm2JavaCompiler;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.Serializable;

public class BRR implements Serializable {
    private String id;
    private Bigraph redex;
    private Bigraph reactum;
    private static final Log LOG = LogFactory.getLog(BRR.class);


    public BRR(String id, Bigraph redex, Bigraph reactum) {
        this.id = id;
        this.redex = redex;
        this.reactum = reactum;

    }

    public BRR(String id, String ruleTerm){
        this.id = id;
        this.redex = ConcreteBgm2JavaCompiler.generate(ruleTerm.split("->")[0] + ";", false).bigraph;
        this.reactum = ConcreteBgm2JavaCompiler.generate(ruleTerm.split("->")[1] + ";", false).bigraph;
    }

    public BRR(String ruleTerm){
        this.redex = ConcreteBgm2JavaCompiler.generate(ruleTerm.split("->")[0] + ";", false).bigraph;
        this.reactum = ConcreteBgm2JavaCompiler.generate(ruleTerm.split("->")[1] + ";", false).bigraph;
    }

    @Override
    public String toString() {
        return redex + " -> " + reactum;
    }

    public String getId() {
        return id;
    }

    public Bigraph getRedex() {
        return redex;
    }

    public Bigraph getReactum() {
        return reactum;
    }
}
