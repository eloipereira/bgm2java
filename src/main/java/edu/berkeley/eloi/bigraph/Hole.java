package edu.berkeley.eloi.bigraph;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: eloipereira
 * Date: 5/16/13
 * Time: 2:49 PM
 * To change this template use File | Settings | File Templates.
 */
public class Hole extends AbstractPlace<Integer> {

    public Hole(Integer id, Place parent) {
        super(id);
        super.parent = parent;
    }


    @Override
    public List<String> getNames() {
        return null;
    }

    @Override
    public void setNames(List<String> names) {

    }

    public String toBgm() {
        return "$" + this.id.toString();
    }
}
