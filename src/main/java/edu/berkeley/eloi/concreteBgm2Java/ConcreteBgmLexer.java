// $ANTLR 3.5 /Users/eloipereira/Dropbox/IDEAWorkspace/Bgm2Java/src/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgm.g 2014-03-25 17:35:30
package edu.berkeley.eloi.concreteBgm2Java;

import org.antlr.runtime.*;

@SuppressWarnings("all")
public class ConcreteBgmLexer extends Lexer {
    public static final int EOF = -1;
    public static final int T__7 = 7;
    public static final int T__8 = 8;
    public static final int T__9 = 9;
    public static final int T__10 = 10;
    public static final int T__11 = 11;
    public static final int T__12 = 12;
    public static final int T__13 = 13;
    public static final int T__14 = 14;
    public static final int T__15 = 15;
    public static final int T__16 = 16;
    public static final int T__17 = 17;
    public static final int T__18 = 18;
    public static final int T__19 = 19;
    public static final int T__20 = 20;
    public static final int T__21 = 21;
    public static final int T__22 = 22;
    public static final int T__23 = 23;
    public static final int T__24 = 24;
    public static final int T__25 = 25;
    public static final int T__26 = 26;
    public static final int T__27 = 27;
    public static final int COMMENT = 4;
    public static final int ID = 5;
    public static final int WS = 6;

    // delegates
    // delegators
    public Lexer[] getDelegates() {
        return new Lexer[]{};
    }

    public ConcreteBgmLexer() {
    }

    public ConcreteBgmLexer(CharStream input) {
        this(input, new RecognizerSharedState());
    }

    public ConcreteBgmLexer(CharStream input, RecognizerSharedState state) {
        super(input, state);
    }

    @Override
    public String getGrammarFileName() {
        return "/Users/eloipereira/Dropbox/IDEAWorkspace/Bgm2Java/src/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgm.g";
    }

    // $ANTLR start "T__7"
    public final void mT__7() throws RecognitionException {
        try {
            int _type = T__7;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /Users/eloipereira/Dropbox/IDEAWorkspace/Bgm2Java/src/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgm.g:9:6: ( ' -> ' )
            // /Users/eloipereira/Dropbox/IDEAWorkspace/Bgm2Java/src/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgm.g:9:8: ' -> '
            {
                match(" -> ");

            }

            state.type = _type;
            state.channel = _channel;
        } finally {
            // do for sure before leaving
        }
    }
    // $ANTLR end "T__7"

    // $ANTLR start "T__8"
    public final void mT__8() throws RecognitionException {
        try {
            int _type = T__8;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /Users/eloipereira/Dropbox/IDEAWorkspace/Bgm2Java/src/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgm.g:10:6: ( '$' )
            // /Users/eloipereira/Dropbox/IDEAWorkspace/Bgm2Java/src/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgm.g:10:8: '$'
            {
                match('$');
            }

            state.type = _type;
            state.channel = _channel;
        } finally {
            // do for sure before leaving
        }
    }
    // $ANTLR end "T__8"

    // $ANTLR start "T__9"
    public final void mT__9() throws RecognitionException {
        try {
            int _type = T__9;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /Users/eloipereira/Dropbox/IDEAWorkspace/Bgm2Java/src/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgm.g:11:6: ( '%active' )
            // /Users/eloipereira/Dropbox/IDEAWorkspace/Bgm2Java/src/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgm.g:11:8: '%active'
            {
                match("%active");

            }

            state.type = _type;
            state.channel = _channel;
        } finally {
            // do for sure before leaving
        }
    }
    // $ANTLR end "T__9"

    // $ANTLR start "T__10"
    public final void mT__10() throws RecognitionException {
        try {
            int _type = T__10;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /Users/eloipereira/Dropbox/IDEAWorkspace/Bgm2Java/src/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgm.g:12:7: ( '%atomic' )
            // /Users/eloipereira/Dropbox/IDEAWorkspace/Bgm2Java/src/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgm.g:12:9: '%atomic'
            {
                match("%atomic");

            }

            state.type = _type;
            state.channel = _channel;
        } finally {
            // do for sure before leaving
        }
    }
    // $ANTLR end "T__10"

    // $ANTLR start "T__11"
    public final void mT__11() throws RecognitionException {
        try {
            int _type = T__11;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /Users/eloipereira/Dropbox/IDEAWorkspace/Bgm2Java/src/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgm.g:13:7: ( '%check' )
            // /Users/eloipereira/Dropbox/IDEAWorkspace/Bgm2Java/src/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgm.g:13:9: '%check'
            {
                match("%check");

            }

            state.type = _type;
            state.channel = _channel;
        } finally {
            // do for sure before leaving
        }
    }
    // $ANTLR end "T__11"

    // $ANTLR start "T__12"
    public final void mT__12() throws RecognitionException {
        try {
            int _type = T__12;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /Users/eloipereira/Dropbox/IDEAWorkspace/Bgm2Java/src/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgm.g:14:7: ( '%name' )
            // /Users/eloipereira/Dropbox/IDEAWorkspace/Bgm2Java/src/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgm.g:14:9: '%name'
            {
                match("%name");

            }

            state.type = _type;
            state.channel = _channel;
        } finally {
            // do for sure before leaving
        }
    }
    // $ANTLR end "T__12"

    // $ANTLR start "T__13"
    public final void mT__13() throws RecognitionException {
        try {
            int _type = T__13;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /Users/eloipereira/Dropbox/IDEAWorkspace/Bgm2Java/src/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgm.g:15:7: ( '%passive' )
            // /Users/eloipereira/Dropbox/IDEAWorkspace/Bgm2Java/src/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgm.g:15:9: '%passive'
            {
                match("%passive");

            }

            state.type = _type;
            state.channel = _channel;
        } finally {
            // do for sure before leaving
        }
    }
    // $ANTLR end "T__13"

    // $ANTLR start "T__14"
    public final void mT__14() throws RecognitionException {
        try {
            int _type = T__14;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /Users/eloipereira/Dropbox/IDEAWorkspace/Bgm2Java/src/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgm.g:16:7: ( '%rule' )
            // /Users/eloipereira/Dropbox/IDEAWorkspace/Bgm2Java/src/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgm.g:16:9: '%rule'
            {
                match("%rule");

            }

            state.type = _type;
            state.channel = _channel;
        } finally {
            // do for sure before leaving
        }
    }
    // $ANTLR end "T__14"

    // $ANTLR start "T__15"
    public final void mT__15() throws RecognitionException {
        try {
            int _type = T__15;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /Users/eloipereira/Dropbox/IDEAWorkspace/Bgm2Java/src/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgm.g:17:7: ( '(' )
            // /Users/eloipereira/Dropbox/IDEAWorkspace/Bgm2Java/src/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgm.g:17:9: '('
            {
                match('(');
            }

            state.type = _type;
            state.channel = _channel;
        } finally {
            // do for sure before leaving
        }
    }
    // $ANTLR end "T__15"

    // $ANTLR start "T__16"
    public final void mT__16() throws RecognitionException {
        try {
            int _type = T__16;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /Users/eloipereira/Dropbox/IDEAWorkspace/Bgm2Java/src/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgm.g:18:7: ( ')' )
            // /Users/eloipereira/Dropbox/IDEAWorkspace/Bgm2Java/src/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgm.g:18:9: ')'
            {
                match(')');
            }

            state.type = _type;
            state.channel = _channel;
        } finally {
            // do for sure before leaving
        }
    }
    // $ANTLR end "T__16"

    // $ANTLR start "T__17"
    public final void mT__17() throws RecognitionException {
        try {
            int _type = T__17;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /Users/eloipereira/Dropbox/IDEAWorkspace/Bgm2Java/src/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgm.g:19:7: ( ',' )
            // /Users/eloipereira/Dropbox/IDEAWorkspace/Bgm2Java/src/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgm.g:19:9: ','
            {
                match(',');
            }

            state.type = _type;
            state.channel = _channel;
        } finally {
            // do for sure before leaving
        }
    }
    // $ANTLR end "T__17"

    // $ANTLR start "T__18"
    public final void mT__18() throws RecognitionException {
        try {
            int _type = T__18;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /Users/eloipereira/Dropbox/IDEAWorkspace/Bgm2Java/src/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgm.g:20:7: ( '-' )
            // /Users/eloipereira/Dropbox/IDEAWorkspace/Bgm2Java/src/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgm.g:20:9: '-'
            {
                match('-');
            }

            state.type = _type;
            state.channel = _channel;
        } finally {
            // do for sure before leaving
        }
    }
    // $ANTLR end "T__18"

    // $ANTLR start "T__19"
    public final void mT__19() throws RecognitionException {
        try {
            int _type = T__19;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /Users/eloipereira/Dropbox/IDEAWorkspace/Bgm2Java/src/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgm.g:21:7: ( '.' )
            // /Users/eloipereira/Dropbox/IDEAWorkspace/Bgm2Java/src/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgm.g:21:9: '.'
            {
                match('.');
            }

            state.type = _type;
            state.channel = _channel;
        } finally {
            // do for sure before leaving
        }
    }
    // $ANTLR end "T__19"

    // $ANTLR start "T__20"
    public final void mT__20() throws RecognitionException {
        try {
            int _type = T__20;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /Users/eloipereira/Dropbox/IDEAWorkspace/Bgm2Java/src/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgm.g:22:7: ( ':' )
            // /Users/eloipereira/Dropbox/IDEAWorkspace/Bgm2Java/src/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgm.g:22:9: ':'
            {
                match(':');
            }

            state.type = _type;
            state.channel = _channel;
        } finally {
            // do for sure before leaving
        }
    }
    // $ANTLR end "T__20"

    // $ANTLR start "T__21"
    public final void mT__21() throws RecognitionException {
        try {
            int _type = T__21;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /Users/eloipereira/Dropbox/IDEAWorkspace/Bgm2Java/src/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgm.g:23:7: ( ';' )
            // /Users/eloipereira/Dropbox/IDEAWorkspace/Bgm2Java/src/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgm.g:23:9: ';'
            {
                match(';');
            }

            state.type = _type;
            state.channel = _channel;
        } finally {
            // do for sure before leaving
        }
    }
    // $ANTLR end "T__21"

    // $ANTLR start "T__22"
    public final void mT__22() throws RecognitionException {
        try {
            int _type = T__22;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /Users/eloipereira/Dropbox/IDEAWorkspace/Bgm2Java/src/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgm.g:24:7: ( '[' )
            // /Users/eloipereira/Dropbox/IDEAWorkspace/Bgm2Java/src/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgm.g:24:9: '['
            {
                match('[');
            }

            state.type = _type;
            state.channel = _channel;
        } finally {
            // do for sure before leaving
        }
    }
    // $ANTLR end "T__22"

    // $ANTLR start "T__23"
    public final void mT__23() throws RecognitionException {
        try {
            int _type = T__23;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /Users/eloipereira/Dropbox/IDEAWorkspace/Bgm2Java/src/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgm.g:25:7: ( ']' )
            // /Users/eloipereira/Dropbox/IDEAWorkspace/Bgm2Java/src/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgm.g:25:9: ']'
            {
                match(']');
            }

            state.type = _type;
            state.channel = _channel;
        } finally {
            // do for sure before leaving
        }
    }
    // $ANTLR end "T__23"

    // $ANTLR start "T__24"
    public final void mT__24() throws RecognitionException {
        try {
            int _type = T__24;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /Users/eloipereira/Dropbox/IDEAWorkspace/Bgm2Java/src/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgm.g:26:7: ( '_' )
            // /Users/eloipereira/Dropbox/IDEAWorkspace/Bgm2Java/src/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgm.g:26:9: '_'
            {
                match('_');
            }

            state.type = _type;
            state.channel = _channel;
        } finally {
            // do for sure before leaving
        }
    }
    // $ANTLR end "T__24"

    // $ANTLR start "T__25"
    public final void mT__25() throws RecognitionException {
        try {
            int _type = T__25;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /Users/eloipereira/Dropbox/IDEAWorkspace/Bgm2Java/src/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgm.g:27:7: ( 'nil' )
            // /Users/eloipereira/Dropbox/IDEAWorkspace/Bgm2Java/src/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgm.g:27:9: 'nil'
            {
                match("nil");

            }

            state.type = _type;
            state.channel = _channel;
        } finally {
            // do for sure before leaving
        }
    }
    // $ANTLR end "T__25"

    // $ANTLR start "T__26"
    public final void mT__26() throws RecognitionException {
        try {
            int _type = T__26;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /Users/eloipereira/Dropbox/IDEAWorkspace/Bgm2Java/src/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgm.g:28:7: ( '|' )
            // /Users/eloipereira/Dropbox/IDEAWorkspace/Bgm2Java/src/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgm.g:28:9: '|'
            {
                match('|');
            }

            state.type = _type;
            state.channel = _channel;
        } finally {
            // do for sure before leaving
        }
    }
    // $ANTLR end "T__26"

    // $ANTLR start "T__27"
    public final void mT__27() throws RecognitionException {
        try {
            int _type = T__27;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /Users/eloipereira/Dropbox/IDEAWorkspace/Bgm2Java/src/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgm.g:29:7: ( '||' )
            // /Users/eloipereira/Dropbox/IDEAWorkspace/Bgm2Java/src/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgm.g:29:9: '||'
            {
                match("||");

            }

            state.type = _type;
            state.channel = _channel;
        } finally {
            // do for sure before leaving
        }
    }
    // $ANTLR end "T__27"

    // $ANTLR start "ID"
    public final void mID() throws RecognitionException {
        try {
            int _type = ID;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /Users/eloipereira/Dropbox/IDEAWorkspace/Bgm2Java/src/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgm.g:34:5: ( ( 'a' .. 'z' | 'A' .. 'Z' | '0' .. '9' )+ ( ( '-' ) ( 'a' .. 'z' | 'A' .. 'Z' | '0' .. '9' )+ )* )
            // /Users/eloipereira/Dropbox/IDEAWorkspace/Bgm2Java/src/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgm.g:34:7: ( 'a' .. 'z' | 'A' .. 'Z' | '0' .. '9' )+ ( ( '-' ) ( 'a' .. 'z' | 'A' .. 'Z' | '0' .. '9' )+ )*
            {
                // /Users/eloipereira/Dropbox/IDEAWorkspace/Bgm2Java/src/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgm.g:34:7: ( 'a' .. 'z' | 'A' .. 'Z' | '0' .. '9' )+
                int cnt1 = 0;
                loop1:
                while (true) {
                    int alt1 = 2;
                    int LA1_0 = input.LA(1);
                    if (((LA1_0 >= '0' && LA1_0 <= '9') || (LA1_0 >= 'A' && LA1_0 <= 'Z') || (LA1_0 >= 'a' && LA1_0 <= 'z'))) {
                        alt1 = 1;
                    }

                    switch (alt1) {
                        case 1:
                            // /Users/eloipereira/Dropbox/IDEAWorkspace/Bgm2Java/src/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgm.g:
                        {
                            if ((input.LA(1) >= '0' && input.LA(1) <= '9') || (input.LA(1) >= 'A' && input.LA(1) <= 'Z') || (input.LA(1) >= 'a' && input.LA(1) <= 'z')) {
                                input.consume();
                            } else {
                                MismatchedSetException mse = new MismatchedSetException(null, input);
                                recover(mse);
                                throw mse;
                            }
                        }
                        break;

                        default:
                            if (cnt1 >= 1) break loop1;
                            EarlyExitException eee = new EarlyExitException(1, input);
                            throw eee;
                    }
                    cnt1++;
                }

                // /Users/eloipereira/Dropbox/IDEAWorkspace/Bgm2Java/src/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgm.g:34:37: ( ( '-' ) ( 'a' .. 'z' | 'A' .. 'Z' | '0' .. '9' )+ )*
                loop3:
                while (true) {
                    int alt3 = 2;
                    int LA3_0 = input.LA(1);
                    if ((LA3_0 == '-')) {
                        alt3 = 1;
                    }

                    switch (alt3) {
                        case 1:
                            // /Users/eloipereira/Dropbox/IDEAWorkspace/Bgm2Java/src/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgm.g:34:38: ( '-' ) ( 'a' .. 'z' | 'A' .. 'Z' | '0' .. '9' )+
                        {
                            // /Users/eloipereira/Dropbox/IDEAWorkspace/Bgm2Java/src/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgm.g:34:38: ( '-' )
                            // /Users/eloipereira/Dropbox/IDEAWorkspace/Bgm2Java/src/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgm.g:34:39: '-'
                            {
                                match('-');
                            }

                            // /Users/eloipereira/Dropbox/IDEAWorkspace/Bgm2Java/src/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgm.g:34:44: ( 'a' .. 'z' | 'A' .. 'Z' | '0' .. '9' )+
                            int cnt2 = 0;
                            loop2:
                            while (true) {
                                int alt2 = 2;
                                int LA2_0 = input.LA(1);
                                if (((LA2_0 >= '0' && LA2_0 <= '9') || (LA2_0 >= 'A' && LA2_0 <= 'Z') || (LA2_0 >= 'a' && LA2_0 <= 'z'))) {
                                    alt2 = 1;
                                }

                                switch (alt2) {
                                    case 1:
                                        // /Users/eloipereira/Dropbox/IDEAWorkspace/Bgm2Java/src/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgm.g:
                                    {
                                        if ((input.LA(1) >= '0' && input.LA(1) <= '9') || (input.LA(1) >= 'A' && input.LA(1) <= 'Z') || (input.LA(1) >= 'a' && input.LA(1) <= 'z')) {
                                            input.consume();
                                        } else {
                                            MismatchedSetException mse = new MismatchedSetException(null, input);
                                            recover(mse);
                                            throw mse;
                                        }
                                    }
                                    break;

                                    default:
                                        if (cnt2 >= 1) break loop2;
                                        EarlyExitException eee = new EarlyExitException(2, input);
                                        throw eee;
                                }
                                cnt2++;
                            }

                        }
                        break;

                        default:
                            break loop3;
                    }
                }

            }

            state.type = _type;
            state.channel = _channel;
        } finally {
            // do for sure before leaving
        }
    }
    // $ANTLR end "ID"

    // $ANTLR start "COMMENT"
    public final void mCOMMENT() throws RecognitionException {
        try {
            int _type = COMMENT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /Users/eloipereira/Dropbox/IDEAWorkspace/Bgm2Java/src/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgm.g:39:3: ( '#' (~ ( '\\r' | '\\n' ) )* )
            // /Users/eloipereira/Dropbox/IDEAWorkspace/Bgm2Java/src/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgm.g:39:6: '#' (~ ( '\\r' | '\\n' ) )*
            {
                match('#');
                // /Users/eloipereira/Dropbox/IDEAWorkspace/Bgm2Java/src/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgm.g:39:10: (~ ( '\\r' | '\\n' ) )*
                loop4:
                while (true) {
                    int alt4 = 2;
                    int LA4_0 = input.LA(1);
                    if (((LA4_0 >= '\u0000' && LA4_0 <= '\t') || (LA4_0 >= '\u000B' && LA4_0 <= '\f') || (LA4_0 >= '\u000E' && LA4_0 <= '\uFFFF'))) {
                        alt4 = 1;
                    }

                    switch (alt4) {
                        case 1:
                            // /Users/eloipereira/Dropbox/IDEAWorkspace/Bgm2Java/src/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgm.g:
                        {
                            if ((input.LA(1) >= '\u0000' && input.LA(1) <= '\t') || (input.LA(1) >= '\u000B' && input.LA(1) <= '\f') || (input.LA(1) >= '\u000E' && input.LA(1) <= '\uFFFF')) {
                                input.consume();
                            } else {
                                MismatchedSetException mse = new MismatchedSetException(null, input);
                                recover(mse);
                                throw mse;
                            }
                        }
                        break;

                        default:
                            break loop4;
                    }
                }

                _channel = HIDDEN;
            }

            state.type = _type;
            state.channel = _channel;
        } finally {
            // do for sure before leaving
        }
    }
    // $ANTLR end "COMMENT"

    // $ANTLR start "WS"
    public final void mWS() throws RecognitionException {
        try {
            int _type = WS;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /Users/eloipereira/Dropbox/IDEAWorkspace/Bgm2Java/src/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgm.g:42:5: ( ( ' ' | '\\t' | '\\r' | '\\n' ) )
            // /Users/eloipereira/Dropbox/IDEAWorkspace/Bgm2Java/src/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgm.g:42:9: ( ' ' | '\\t' | '\\r' | '\\n' )
            {
                if ((input.LA(1) >= '\t' && input.LA(1) <= '\n') || input.LA(1) == '\r' || input.LA(1) == ' ') {
                    input.consume();
                } else {
                    MismatchedSetException mse = new MismatchedSetException(null, input);
                    recover(mse);
                    throw mse;
                }
                _channel = HIDDEN;
            }

            state.type = _type;
            state.channel = _channel;
        } finally {
            // do for sure before leaving
        }
    }
    // $ANTLR end "WS"

    @Override
    public void mTokens() throws RecognitionException {
        // /Users/eloipereira/Dropbox/IDEAWorkspace/Bgm2Java/src/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgm.g:1:8: ( T__7 | T__8 | T__9 | T__10 | T__11 | T__12 | T__13 | T__14 | T__15 | T__16 | T__17 | T__18 | T__19 | T__20 | T__21 | T__22 | T__23 | T__24 | T__25 | T__26 | T__27 | ID | COMMENT | WS )
        int alt5 = 24;
        switch (input.LA(1)) {
            case ' ': {
                int LA5_1 = input.LA(2);
                if ((LA5_1 == '-')) {
                    alt5 = 1;
                } else {
                    alt5 = 24;
                }

            }
            break;
            case '$': {
                alt5 = 2;
            }
            break;
            case '%': {
                switch (input.LA(2)) {
                    case 'a': {
                        int LA5_20 = input.LA(3);
                        if ((LA5_20 == 'c')) {
                            alt5 = 3;
                        } else if ((LA5_20 == 't')) {
                            alt5 = 4;
                        } else {
                            int nvaeMark = input.mark();
                            try {
                                for (int nvaeConsume = 0; nvaeConsume < 3 - 1; nvaeConsume++) {
                                    input.consume();
                                }
                                NoViableAltException nvae =
                                        new NoViableAltException("", 5, 20, input);
                                throw nvae;
                            } finally {
                                input.rewind(nvaeMark);
                            }
                        }

                    }
                    break;
                    case 'c': {
                        alt5 = 5;
                    }
                    break;
                    case 'n': {
                        alt5 = 6;
                    }
                    break;
                    case 'p': {
                        alt5 = 7;
                    }
                    break;
                    case 'r': {
                        alt5 = 8;
                    }
                    break;
                    default:
                        int nvaeMark = input.mark();
                        try {
                            input.consume();
                            NoViableAltException nvae =
                                    new NoViableAltException("", 5, 3, input);
                            throw nvae;
                        } finally {
                            input.rewind(nvaeMark);
                        }
                }
            }
            break;
            case '(': {
                alt5 = 9;
            }
            break;
            case ')': {
                alt5 = 10;
            }
            break;
            case ',': {
                alt5 = 11;
            }
            break;
            case '-': {
                alt5 = 12;
            }
            break;
            case '.': {
                alt5 = 13;
            }
            break;
            case ':': {
                alt5 = 14;
            }
            break;
            case ';': {
                alt5 = 15;
            }
            break;
            case '[': {
                alt5 = 16;
            }
            break;
            case ']': {
                alt5 = 17;
            }
            break;
            case '_': {
                alt5 = 18;
            }
            break;
            case 'n': {
                int LA5_14 = input.LA(2);
                if ((LA5_14 == 'i')) {
                    int LA5_25 = input.LA(3);
                    if ((LA5_25 == 'l')) {
                        int LA5_30 = input.LA(4);
                        if ((LA5_30 == '-' || (LA5_30 >= '0' && LA5_30 <= '9') || (LA5_30 >= 'A' && LA5_30 <= 'Z') || (LA5_30 >= 'a' && LA5_30 <= 'z'))) {
                            alt5 = 22;
                        } else {
                            alt5 = 19;
                        }

                    } else {
                        alt5 = 22;
                    }

                } else {
                    alt5 = 22;
                }

            }
            break;
            case '|': {
                int LA5_15 = input.LA(2);
                if ((LA5_15 == '|')) {
                    alt5 = 21;
                } else {
                    alt5 = 20;
                }

            }
            break;
            case '0':
            case '1':
            case '2':
            case '3':
            case '4':
            case '5':
            case '6':
            case '7':
            case '8':
            case '9':
            case 'A':
            case 'B':
            case 'C':
            case 'D':
            case 'E':
            case 'F':
            case 'G':
            case 'H':
            case 'I':
            case 'J':
            case 'K':
            case 'L':
            case 'M':
            case 'N':
            case 'O':
            case 'P':
            case 'Q':
            case 'R':
            case 'S':
            case 'T':
            case 'U':
            case 'V':
            case 'W':
            case 'X':
            case 'Y':
            case 'Z':
            case 'a':
            case 'b':
            case 'c':
            case 'd':
            case 'e':
            case 'f':
            case 'g':
            case 'h':
            case 'i':
            case 'j':
            case 'k':
            case 'l':
            case 'm':
            case 'o':
            case 'p':
            case 'q':
            case 'r':
            case 's':
            case 't':
            case 'u':
            case 'v':
            case 'w':
            case 'x':
            case 'y':
            case 'z': {
                alt5 = 22;
            }
            break;
            case '#': {
                alt5 = 23;
            }
            break;
            case '\t':
            case '\n':
            case '\r': {
                alt5 = 24;
            }
            break;
            default:
                NoViableAltException nvae =
                        new NoViableAltException("", 5, 0, input);
                throw nvae;
        }
        switch (alt5) {
            case 1:
                // /Users/eloipereira/Dropbox/IDEAWorkspace/Bgm2Java/src/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgm.g:1:10: T__7
            {
                mT__7();

            }
            break;
            case 2:
                // /Users/eloipereira/Dropbox/IDEAWorkspace/Bgm2Java/src/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgm.g:1:15: T__8
            {
                mT__8();

            }
            break;
            case 3:
                // /Users/eloipereira/Dropbox/IDEAWorkspace/Bgm2Java/src/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgm.g:1:20: T__9
            {
                mT__9();

            }
            break;
            case 4:
                // /Users/eloipereira/Dropbox/IDEAWorkspace/Bgm2Java/src/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgm.g:1:25: T__10
            {
                mT__10();

            }
            break;
            case 5:
                // /Users/eloipereira/Dropbox/IDEAWorkspace/Bgm2Java/src/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgm.g:1:31: T__11
            {
                mT__11();

            }
            break;
            case 6:
                // /Users/eloipereira/Dropbox/IDEAWorkspace/Bgm2Java/src/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgm.g:1:37: T__12
            {
                mT__12();

            }
            break;
            case 7:
                // /Users/eloipereira/Dropbox/IDEAWorkspace/Bgm2Java/src/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgm.g:1:43: T__13
            {
                mT__13();

            }
            break;
            case 8:
                // /Users/eloipereira/Dropbox/IDEAWorkspace/Bgm2Java/src/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgm.g:1:49: T__14
            {
                mT__14();

            }
            break;
            case 9:
                // /Users/eloipereira/Dropbox/IDEAWorkspace/Bgm2Java/src/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgm.g:1:55: T__15
            {
                mT__15();

            }
            break;
            case 10:
                // /Users/eloipereira/Dropbox/IDEAWorkspace/Bgm2Java/src/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgm.g:1:61: T__16
            {
                mT__16();

            }
            break;
            case 11:
                // /Users/eloipereira/Dropbox/IDEAWorkspace/Bgm2Java/src/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgm.g:1:67: T__17
            {
                mT__17();

            }
            break;
            case 12:
                // /Users/eloipereira/Dropbox/IDEAWorkspace/Bgm2Java/src/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgm.g:1:73: T__18
            {
                mT__18();

            }
            break;
            case 13:
                // /Users/eloipereira/Dropbox/IDEAWorkspace/Bgm2Java/src/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgm.g:1:79: T__19
            {
                mT__19();

            }
            break;
            case 14:
                // /Users/eloipereira/Dropbox/IDEAWorkspace/Bgm2Java/src/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgm.g:1:85: T__20
            {
                mT__20();

            }
            break;
            case 15:
                // /Users/eloipereira/Dropbox/IDEAWorkspace/Bgm2Java/src/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgm.g:1:91: T__21
            {
                mT__21();

            }
            break;
            case 16:
                // /Users/eloipereira/Dropbox/IDEAWorkspace/Bgm2Java/src/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgm.g:1:97: T__22
            {
                mT__22();

            }
            break;
            case 17:
                // /Users/eloipereira/Dropbox/IDEAWorkspace/Bgm2Java/src/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgm.g:1:103: T__23
            {
                mT__23();

            }
            break;
            case 18:
                // /Users/eloipereira/Dropbox/IDEAWorkspace/Bgm2Java/src/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgm.g:1:109: T__24
            {
                mT__24();

            }
            break;
            case 19:
                // /Users/eloipereira/Dropbox/IDEAWorkspace/Bgm2Java/src/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgm.g:1:115: T__25
            {
                mT__25();

            }
            break;
            case 20:
                // /Users/eloipereira/Dropbox/IDEAWorkspace/Bgm2Java/src/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgm.g:1:121: T__26
            {
                mT__26();

            }
            break;
            case 21:
                // /Users/eloipereira/Dropbox/IDEAWorkspace/Bgm2Java/src/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgm.g:1:127: T__27
            {
                mT__27();

            }
            break;
            case 22:
                // /Users/eloipereira/Dropbox/IDEAWorkspace/Bgm2Java/src/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgm.g:1:133: ID
            {
                mID();

            }
            break;
            case 23:
                // /Users/eloipereira/Dropbox/IDEAWorkspace/Bgm2Java/src/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgm.g:1:136: COMMENT
            {
                mCOMMENT();

            }
            break;
            case 24:
                // /Users/eloipereira/Dropbox/IDEAWorkspace/Bgm2Java/src/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgm.g:1:144: WS
            {
                mWS();

            }
            break;

        }
    }


}
