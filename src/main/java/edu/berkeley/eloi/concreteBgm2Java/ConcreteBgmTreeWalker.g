tree grammar ConcreteBgmTreeWalker;

options {
  	language = Java;
  	tokenVocab = ConcreteBgm;
  	ASTLabelType = CommonTree;
}

@header {
  	package edu.berkeley.eloi.concreteBgm2Java;
  	import edu.berkeley.eloi.bigraph.*;
  	import java.util.List;
  	import java.io.IOException;
}

@members{
	private PlaceList nodes = new PlaceList();
  	private PlaceList nodesTmp = new PlaceList();
  	private boolean isModel = false;
  	private boolean isRule = false;
  	private int regionCounter = 0;
  	//private Place parent = new Region(regionCounter);
  	private PlaceList parents = new PlaceList();
  	
}

program returns [List<Control> signature, List<String> names, List<BRR> rules, Bigraph bigraph]
@init{
	nodes.add(new Region(regionCounter));
	retval.signature = new ArrayList<Control>();
	retval.names = new ArrayList<String>();
	retval.rules = new ArrayList<BRR>();
	parents.add(new Region(regionCounter));
}
: 
	s=signature {
		retval.signature.addAll($s.signature);
	} 
	n=names {
		retval.names.addAll($n.names);
	} 
	r=rules {
		retval.rules.addAll($r.rules);
	} 
	b=model {
		retval.bigraph = $b.bigraph;
	}
;

signature returns [List<Control> signature] 
@init{
	signature = new ArrayList<Control>();
}
: 
(	
	c=control 
	{signature.add($c.control);}
)*;

names returns [List<String> names] 
@init{
	names = new ArrayList<String>();
}
: 
(
	n=name 
	{names.add($n.name);}
)*;

rules returns [List<BRR> rules] 
@init{
	rules = new ArrayList<BRR>();
}
: 
(
	r=rule 
	{rules.add($r.rule);}
)*;

control returns [Control control]
 : ^('%active' i0=ID i1=ID i2=ID) {control = new Control($i0.text + "_" + $i1.text, Integer.parseInt($i2.text), new Control.Active());}
	| ^('%passive' i0=ID i1=ID i2=ID) {control = new Control($i0.text + "_" + $i1.text, Integer.parseInt($i2.text), new Control.Passive());}
	| ^('%atomic' i0=ID i1=ID i2=ID) {control = new Control($i0.text + "_" + $i1.text, Integer.parseInt($i2.text), new Control.Atomic());}
;

name returns [String name]: ^('%name' ID){name = $ID.text;};

rule returns [BRR rule]
@init{
	isRule = true;
      	nodesTmp.clear();
}
: 
^('%rule' ID t0=termPar t1=termPar) 
{
	
	rule = new BRR($ID.text,$t0.termResult + " -> " + $t1.termResult + ";");
	isRule = false;
};

model returns [Bigraph bigraph]
@init{
	isModel = true;
}
: 
termPar {
	bigraph = new Bigraph(nodes);
	isModel = false;
};

termPar returns [String termResult] 
: 
t0=term {
	termResult=$t0.termResult;
} 
(('|' {
	termResult+="|";
	} 
| '||' {
	termResult+="||";
	regionCounter++;
	parents.clear();
	parents.add(new Region(regionCounter));
	nodes.add(new Region(regionCounter));
	}) 
t1=term {termResult+=$t1.termResult;}
)*;

term returns [String termResult, PlaceList nodesResult]
@init{
	retval.nodesResult = new PlaceList();
}
: n=node {
	retval.termResult=$n.result;
	BigraphNode nodeTmp = $n.node;
} 
({
	//parent = nodeTmp;
	parents.add(nodeTmp);
} '.' t0=term {
	retval.termResult += '.' + $t0.termResult;
	//parent = new Region(regionCounter);
	if(parents.size()>1){ 
		parents.remove(parents.size()-1);
	}
})?
{
 retval.nodesResult.add(nodeTmp);
 if(isModel) {nodes.add(nodeTmp);};
 if(isRule && !nodesTmp.contains(nodeTmp)) {nodesTmp.add(nodeTmp);};
}
| '(' t1=termPar ')' {retval.termResult='(' + $t1.termResult + ')'; } 
| '$'ID {retval.termResult='$'+$ID.text;
          if(isModel) {nodes.add(new Hole(Integer.parseInt($ID.text),parents.get(parents.size()-1)));};
} 
| 'nil' {retval.termResult="nil";};

node returns [String result, BigraphNode node] 
@init{
      retval.result="";
      List<String> names = new ArrayList<String>();
      String id;
      String ctrId;
} 
:  
	i0=ID {retval.result+=$i0.text; id=$i0.text;} 
	  '_' 
	i1=ID {retval.result+="_"+$i1.text;ctrId=$i1.text;}
         ('[' {retval.result+='[';} 
        n0=nameOrDash {retval.result+=$n0.result; names.add($n0.result);} 
         (',' {retval.result+=',';} 
        n1=nameOrDash {retval.result+=$n1.result;names.add($n1.result);})* 
	  ']' {retval.result+=']';} )? 
{BigraphNode n = new BigraphNode(id, ctrId, names, parents.get(parents.size()-1));
 retval.node = n;}
;

nameOrDash returns [String result] : ID {result = $ID.text;} | '-' {result = "-";};
