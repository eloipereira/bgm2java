package edu.berkeley.eloi.concreteBgm2Java;

import org.antlr.runtime.*;
import org.antlr.runtime.tree.CommonTree;
import org.antlr.runtime.tree.CommonTreeNodeStream;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.IOException;

public final class ConcreteBgm2JavaCompiler {

    private static final Log LOG = LogFactory.getLog(ConcreteBgm2JavaCompiler.class);

    private ConcreteBgm2JavaCompiler(){}

    public static ConcreteBgmTreeWalker.program_return generate(String bgm, boolean isFile)  {
        try {
            CharStream input;
            if (isFile) {
                input = new ANTLRFileStream(bgm);
            } else {
                input = new ANTLRStringStream(bgm);
            }
            ConcreteBgmLexer lexer = new ConcreteBgmLexer(input);
            CommonTokenStream tokens = new CommonTokenStream(lexer);
            ConcreteBgmParser parser = new ConcreteBgmParser(tokens);
            ConcreteBgmParser.program_return res = parser.program();
            CommonTree tree = (CommonTree) res.getTree();
            LOG.debug("AST of expression \"" + input + "\"\n" + tree.toStringTree());
            CommonTreeNodeStream nodeStream = new CommonTreeNodeStream(tree);
            ConcreteBgmTreeWalker walker = new ConcreteBgmTreeWalker(nodeStream);
            LOG.debug("Interpreting expression \"" + input + "\"");
            return walker.program();
        } catch (IOException e){
            LOG.error(e);
            return null;
        } catch (RecognitionException e){
            LOG.error(e);
            return null;
        }
    }
}
