grammar ConcreteBgm;

options {language = Java;
	 output = AST;
  	 ASTLabelType=CommonTree;} 

@header {package edu.berkeley.eloi.concreteBgm2Java;}
@lexer::header {package edu.berkeley.eloi.concreteBgm2Java;}

program : signature names rules model ('%check'!)? (';'!)? (EOF!);

signature : control*;

names 	: name*;

rules	: rule*;

control : (('%active'^ | '%passive'^ | '%atomic'^) ID '_'! ID ':'! ID ';'!);

name 	: ('%name'^ ID ';'!);

rule 	: ('%rule'^ ID termPar ' -> '! termPar ';'!);

model 	:  termPar  ';'!;

termPar : term (('|' | '||') term)*;

term 	:  node ('.' term )? | '(' termPar ')' | '$'ID | 'nil';

node 	: ID '_' ID ('[' nameOrDash (',' nameOrDash)* ']')?;

nameOrDash : ID | '-';

ID  :	('a'..'z'|'A'..'Z'|'0'..'9')+ (('-') ('a'..'z'|'A'..'Z'|'0'..'9')+)*
    ;


COMMENT
  :  '#' ~( '\r' | '\n')* {$channel=HIDDEN;}
  ;
  
WS  :   ( ' '
        | '\t'
        | '\r'
        | '\n'
        ) {$channel=HIDDEN;}
    ;

