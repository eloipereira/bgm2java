// $ANTLR 3.5.1 /home/eloi/Dropbox/IDEAWorkspace/Bgm2Java/src/main/java/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgmTreeWalker.g 2014-08-13 15:21:18

  	package edu.berkeley.eloi.concreteBgm2Java;

import edu.berkeley.eloi.bigraph.*;
import org.antlr.runtime.*;
import org.antlr.runtime.tree.CommonTree;
import org.antlr.runtime.tree.TreeNodeStream;
import org.antlr.runtime.tree.TreeParser;
import org.antlr.runtime.tree.TreeRuleReturnScope;

import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("all")
public class ConcreteBgmTreeWalker extends TreeParser {
	public static final String[] tokenNames = new String[] {
		"<invalid>", "<EOR>", "<DOWN>", "<UP>", "COMMENT", "ID", "WS", "' -> '", 
		"'$'", "'%active'", "'%atomic'", "'%check'", "'%name'", "'%passive'", 
		"'%rule'", "'('", "')'", "','", "'-'", "'.'", "':'", "';'", "'['", "']'", 
		"'_'", "'nil'", "'|'", "'||'"
	};
	public static final int EOF=-1;
	public static final int T__7=7;
	public static final int T__8=8;
	public static final int T__9=9;
	public static final int T__10=10;
	public static final int T__11=11;
	public static final int T__12=12;
	public static final int T__13=13;
	public static final int T__14=14;
	public static final int T__15=15;
	public static final int T__16=16;
	public static final int T__17=17;
	public static final int T__18=18;
	public static final int T__19=19;
	public static final int T__20=20;
	public static final int T__21=21;
	public static final int T__22=22;
	public static final int T__23=23;
	public static final int T__24=24;
	public static final int T__25=25;
	public static final int T__26=26;
	public static final int T__27=27;
	public static final int COMMENT=4;
	public static final int ID=5;
	public static final int WS=6;

	// delegates
	public TreeParser[] getDelegates() {
		return new TreeParser[] {};
	}

	// delegators


	public ConcreteBgmTreeWalker(TreeNodeStream input) {
		this(input, new RecognizerSharedState());
	}
	public ConcreteBgmTreeWalker(TreeNodeStream input, RecognizerSharedState state) {
		super(input, state);
	}

	@Override public String[] getTokenNames() { return ConcreteBgmTreeWalker.tokenNames; }
	@Override public String getGrammarFileName() { return "/home/eloi/Dropbox/IDEAWorkspace/Bgm2Java/src/main/java/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgmTreeWalker.g"; }


		private PlaceList nodes = new PlaceList();
	  	private PlaceList nodesTmp = new PlaceList();
	  	private boolean isModel = false;
	  	private boolean isRule = false;
	  	private int regionCounter = 0;
	  	//private Place parent = new Region(regionCounter);
	  	private PlaceList parents = new PlaceList();
	  	


	public static class program_return extends TreeRuleReturnScope {
		public List<Control> signature;
		public List<String> names;
		public List<BRR> rules;
		public Bigraph bigraph;
	};


	// $ANTLR start "program"
	// /home/eloi/Dropbox/IDEAWorkspace/Bgm2Java/src/main/java/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgmTreeWalker.g:27:1: program returns [List<Control> signature, List<String> names, List<BRR> rules, Bigraph bigraph] : s= signature n= names r= rules b= model ;
	public final ConcreteBgmTreeWalker.program_return program() throws RecognitionException {
		ConcreteBgmTreeWalker.program_return retval = new ConcreteBgmTreeWalker.program_return();
		retval.start = input.LT(1);

		List<Control> s =null;
		List<String> n =null;
		List<BRR> r =null;
		Bigraph b =null;


			nodes.add(new Region(regionCounter));
			retval.signature = new ArrayList<Control>();
			retval.names = new ArrayList<String>();
			retval.rules = new ArrayList<BRR>();
			parents.add(new Region(regionCounter));

		try {
			// /home/eloi/Dropbox/IDEAWorkspace/Bgm2Java/src/main/java/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgmTreeWalker.g:35:2: (s= signature n= names r= rules b= model )
			// /home/eloi/Dropbox/IDEAWorkspace/Bgm2Java/src/main/java/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgmTreeWalker.g:36:2: s= signature n= names r= rules b= model
			{
			pushFollow(FOLLOW_signature_in_program70);
			s=signature();
			state._fsp--;


					retval.signature.addAll(s);
				
			pushFollow(FOLLOW_names_in_program78);
			n=names();
			state._fsp--;


					retval.names.addAll(n);
				
			pushFollow(FOLLOW_rules_in_program86);
			r=rules();
			state._fsp--;


					retval.rules.addAll(r);
				
			pushFollow(FOLLOW_model_in_program94);
			b=model();
			state._fsp--;


					retval.bigraph = b;
				
			}

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "program"



	// $ANTLR start "signature"
	// /home/eloi/Dropbox/IDEAWorkspace/Bgm2Java/src/main/java/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgmTreeWalker.g:50:1: signature returns [List<Control> signature] : (c= control )* ;
	public final List<Control> signature() throws RecognitionException {
		List<Control> signature = null;


		Control c =null;


			signature = new ArrayList<Control>();

		try {
			// /home/eloi/Dropbox/IDEAWorkspace/Bgm2Java/src/main/java/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgmTreeWalker.g:54:2: ( (c= control )* )
			// /home/eloi/Dropbox/IDEAWorkspace/Bgm2Java/src/main/java/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgmTreeWalker.g:55:2: (c= control )*
			{
			// /home/eloi/Dropbox/IDEAWorkspace/Bgm2Java/src/main/java/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgmTreeWalker.g:55:2: (c= control )*
			loop1:
			while (true) {
				int alt1=2;
				int LA1_0 = input.LA(1);
				if ( ((LA1_0 >= 9 && LA1_0 <= 10)||LA1_0==13) ) {
					alt1=1;
				}

				switch (alt1) {
				case 1 :
					// /home/eloi/Dropbox/IDEAWorkspace/Bgm2Java/src/main/java/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgmTreeWalker.g:56:2: c= control
					{
					pushFollow(FOLLOW_control_in_signature121);
					c=control();
					state._fsp--;

					signature.add(c);
					}
					break;

				default :
					break loop1;
				}
			}

			}

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return signature;
	}
	// $ANTLR end "signature"



	// $ANTLR start "names"
	// /home/eloi/Dropbox/IDEAWorkspace/Bgm2Java/src/main/java/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgmTreeWalker.g:60:1: names returns [List<String> names] : (n= name )* ;
	public final List<String> names() throws RecognitionException {
		List<String> names = null;


		String n =null;


			names = new ArrayList<String>();

		try {
			// /home/eloi/Dropbox/IDEAWorkspace/Bgm2Java/src/main/java/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgmTreeWalker.g:64:2: ( (n= name )* )
			// /home/eloi/Dropbox/IDEAWorkspace/Bgm2Java/src/main/java/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgmTreeWalker.g:65:2: (n= name )*
			{
			// /home/eloi/Dropbox/IDEAWorkspace/Bgm2Java/src/main/java/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgmTreeWalker.g:65:2: (n= name )*
			loop2:
			while (true) {
				int alt2=2;
				int LA2_0 = input.LA(1);
				if ( (LA2_0==12) ) {
					alt2=1;
				}

				switch (alt2) {
				case 1 :
					// /home/eloi/Dropbox/IDEAWorkspace/Bgm2Java/src/main/java/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgmTreeWalker.g:66:2: n= name
					{
					pushFollow(FOLLOW_name_in_names151);
					n=name();
					state._fsp--;

					names.add(n);
					}
					break;

				default :
					break loop2;
				}
			}

			}

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return names;
	}
	// $ANTLR end "names"



	// $ANTLR start "rules"
	// /home/eloi/Dropbox/IDEAWorkspace/Bgm2Java/src/main/java/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgmTreeWalker.g:70:1: rules returns [List<BRR> rules] : (r= rule )* ;
	public final List<BRR> rules() throws RecognitionException {
		List<BRR> rules = null;


		BRR r =null;


			rules = new ArrayList<BRR>();

		try {
			// /home/eloi/Dropbox/IDEAWorkspace/Bgm2Java/src/main/java/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgmTreeWalker.g:74:2: ( (r= rule )* )
			// /home/eloi/Dropbox/IDEAWorkspace/Bgm2Java/src/main/java/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgmTreeWalker.g:75:2: (r= rule )*
			{
			// /home/eloi/Dropbox/IDEAWorkspace/Bgm2Java/src/main/java/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgmTreeWalker.g:75:2: (r= rule )*
			loop3:
			while (true) {
				int alt3=2;
				int LA3_0 = input.LA(1);
				if ( (LA3_0==14) ) {
					alt3=1;
				}

				switch (alt3) {
				case 1 :
					// /home/eloi/Dropbox/IDEAWorkspace/Bgm2Java/src/main/java/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgmTreeWalker.g:76:2: r= rule
					{
					pushFollow(FOLLOW_rule_in_rules181);
					r=rule();
					state._fsp--;

					rules.add(r);
					}
					break;

				default :
					break loop3;
				}
			}

			}

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return rules;
	}
	// $ANTLR end "rules"



	// $ANTLR start "control"
	// /home/eloi/Dropbox/IDEAWorkspace/Bgm2Java/src/main/java/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgmTreeWalker.g:80:1: control returns [Control control] : ( ^( '%active' i0= ID i1= ID i2= ID ) | ^( '%passive' i0= ID i1= ID i2= ID ) | ^( '%atomic' i0= ID i1= ID i2= ID ) );
	public final Control control() throws RecognitionException {
		Control control = null;


		CommonTree i0=null;
		CommonTree i1=null;
		CommonTree i2=null;

		try {
			// /home/eloi/Dropbox/IDEAWorkspace/Bgm2Java/src/main/java/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgmTreeWalker.g:81:2: ( ^( '%active' i0= ID i1= ID i2= ID ) | ^( '%passive' i0= ID i1= ID i2= ID ) | ^( '%atomic' i0= ID i1= ID i2= ID ) )
			int alt4=3;
			switch ( input.LA(1) ) {
			case 9:
				{
				alt4=1;
				}
				break;
			case 13:
				{
				alt4=2;
				}
				break;
			case 10:
				{
				alt4=3;
				}
				break;
			default:
				NoViableAltException nvae =
					new NoViableAltException("", 4, 0, input);
				throw nvae;
			}
			switch (alt4) {
				case 1 :
					// /home/eloi/Dropbox/IDEAWorkspace/Bgm2Java/src/main/java/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgmTreeWalker.g:81:4: ^( '%active' i0= ID i1= ID i2= ID )
					{
					match(input,9,FOLLOW_9_in_control202); 
					match(input, Token.DOWN, null); 
					i0=(CommonTree)match(input,ID,FOLLOW_ID_in_control206); 
					i1=(CommonTree)match(input,ID,FOLLOW_ID_in_control210); 
					i2=(CommonTree)match(input,ID,FOLLOW_ID_in_control214); 
					match(input, Token.UP, null); 

					control = new Control((i0!=null?i0.getText():null) + "_" + (i1!=null?i1.getText():null), Integer.parseInt((i2!=null?i2.getText():null)), new Control.Active());
					}
					break;
				case 2 :
					// /home/eloi/Dropbox/IDEAWorkspace/Bgm2Java/src/main/java/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgmTreeWalker.g:82:4: ^( '%passive' i0= ID i1= ID i2= ID )
					{
					match(input,13,FOLLOW_13_in_control223); 
					match(input, Token.DOWN, null); 
					i0=(CommonTree)match(input,ID,FOLLOW_ID_in_control227); 
					i1=(CommonTree)match(input,ID,FOLLOW_ID_in_control231); 
					i2=(CommonTree)match(input,ID,FOLLOW_ID_in_control235); 
					match(input, Token.UP, null); 

					control = new Control((i0!=null?i0.getText():null) + "_" + (i1!=null?i1.getText():null), Integer.parseInt((i2!=null?i2.getText():null)), new Control.Passive());
					}
					break;
				case 3 :
					// /home/eloi/Dropbox/IDEAWorkspace/Bgm2Java/src/main/java/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgmTreeWalker.g:83:4: ^( '%atomic' i0= ID i1= ID i2= ID )
					{
					match(input,10,FOLLOW_10_in_control244); 
					match(input, Token.DOWN, null); 
					i0=(CommonTree)match(input,ID,FOLLOW_ID_in_control248); 
					i1=(CommonTree)match(input,ID,FOLLOW_ID_in_control252); 
					i2=(CommonTree)match(input,ID,FOLLOW_ID_in_control256); 
					match(input, Token.UP, null); 

					control = new Control((i0!=null?i0.getText():null) + "_" + (i1!=null?i1.getText():null), Integer.parseInt((i2!=null?i2.getText():null)), new Control.Atomic());
					}
					break;

			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return control;
	}
	// $ANTLR end "control"



	// $ANTLR start "name"
	// /home/eloi/Dropbox/IDEAWorkspace/Bgm2Java/src/main/java/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgmTreeWalker.g:86:1: name returns [String name] : ^( '%name' ID ) ;
	public final String name() throws RecognitionException {
		String name = null;


		CommonTree ID1=null;

		try {
			// /home/eloi/Dropbox/IDEAWorkspace/Bgm2Java/src/main/java/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgmTreeWalker.g:86:27: ( ^( '%name' ID ) )
			// /home/eloi/Dropbox/IDEAWorkspace/Bgm2Java/src/main/java/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgmTreeWalker.g:86:29: ^( '%name' ID )
			{
			match(input,12,FOLLOW_12_in_name272); 
			match(input, Token.DOWN, null); 
			ID1=(CommonTree)match(input,ID,FOLLOW_ID_in_name274); 
			match(input, Token.UP, null); 

			name = (ID1!=null?ID1.getText():null);
			}

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return name;
	}
	// $ANTLR end "name"



	// $ANTLR start "rule"
	// /home/eloi/Dropbox/IDEAWorkspace/Bgm2Java/src/main/java/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgmTreeWalker.g:88:1: rule returns [BRR rule] : ^( '%rule' ID t0= termPar t1= termPar ) ;
	public final BRR rule() throws RecognitionException {
		BRR rule = null;


		CommonTree ID2=null;
		String t0 =null;
		String t1 =null;


			isRule = true;
		      	nodesTmp.clear();

		try {
			// /home/eloi/Dropbox/IDEAWorkspace/Bgm2Java/src/main/java/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgmTreeWalker.g:93:3: ( ^( '%rule' ID t0= termPar t1= termPar ) )
			// /home/eloi/Dropbox/IDEAWorkspace/Bgm2Java/src/main/java/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgmTreeWalker.g:94:3: ^( '%rule' ID t0= termPar t1= termPar )
			{
			match(input,14,FOLLOW_14_in_rule294); 
			match(input, Token.DOWN, null); 
			ID2=(CommonTree)match(input,ID,FOLLOW_ID_in_rule296); 
			pushFollow(FOLLOW_termPar_in_rule300);
			t0=termPar();
			state._fsp--;

			pushFollow(FOLLOW_termPar_in_rule304);
			t1=termPar();
			state._fsp--;

			match(input, Token.UP, null); 


				
				rule = new BRR((ID2!=null?ID2.getText():null),t0 + " -> " + t1 + ";");
				isRule = false;

			}

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return rule;
	}
	// $ANTLR end "rule"



	// $ANTLR start "model"
	// /home/eloi/Dropbox/IDEAWorkspace/Bgm2Java/src/main/java/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgmTreeWalker.g:101:1: model returns [Bigraph bigraph] : termPar ;
	public final Bigraph model() throws RecognitionException {
		Bigraph bigraph = null;



			isModel = true;

		try {
			// /home/eloi/Dropbox/IDEAWorkspace/Bgm2Java/src/main/java/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgmTreeWalker.g:105:1: ( termPar )
			// /home/eloi/Dropbox/IDEAWorkspace/Bgm2Java/src/main/java/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgmTreeWalker.g:106:1: termPar
			{
			pushFollow(FOLLOW_termPar_in_model325);
			termPar();
			state._fsp--;


				bigraph = new Bigraph(nodes);
				isModel = false;

			}

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return bigraph;
	}
	// $ANTLR end "model"



	// $ANTLR start "termPar"
	// /home/eloi/Dropbox/IDEAWorkspace/Bgm2Java/src/main/java/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgmTreeWalker.g:111:1: termPar returns [String termResult] : t0= term ( ( '|' | '||' ) t1= term )* ;
	public final String termPar() throws RecognitionException {
		String termResult = null;


		TreeRuleReturnScope t0 =null;
		TreeRuleReturnScope t1 =null;

		try {
			// /home/eloi/Dropbox/IDEAWorkspace/Bgm2Java/src/main/java/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgmTreeWalker.g:112:3: (t0= term ( ( '|' | '||' ) t1= term )* )
			// /home/eloi/Dropbox/IDEAWorkspace/Bgm2Java/src/main/java/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgmTreeWalker.g:113:3: t0= term ( ( '|' | '||' ) t1= term )*
			{
			pushFollow(FOLLOW_term_in_termPar343);
			t0=term();
			state._fsp--;


				termResult=(t0!=null?((ConcreteBgmTreeWalker.term_return)t0).termResult:null);

			// /home/eloi/Dropbox/IDEAWorkspace/Bgm2Java/src/main/java/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgmTreeWalker.g:116:2: ( ( '|' | '||' ) t1= term )*
			loop6:
			while (true) {
				int alt6=2;
				int LA6_0 = input.LA(1);
				if ( ((LA6_0 >= 26 && LA6_0 <= 27)) ) {
					alt6=1;
				}

				switch (alt6) {
				case 1 :
					// /home/eloi/Dropbox/IDEAWorkspace/Bgm2Java/src/main/java/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgmTreeWalker.g:116:2: ( '|' | '||' ) t1= term
					{
					// /home/eloi/Dropbox/IDEAWorkspace/Bgm2Java/src/main/java/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgmTreeWalker.g:116:2: ( '|' | '||' )
					int alt5=2;
					int LA5_0 = input.LA(1);
					if ( (LA5_0==26) ) {
						alt5=1;
					}
					else if ( (LA5_0==27) ) {
						alt5=2;
					}

					else {
						NoViableAltException nvae =
							new NoViableAltException("", 5, 0, input);
						throw nvae;
					}

					switch (alt5) {
						case 1 :
							// /home/eloi/Dropbox/IDEAWorkspace/Bgm2Java/src/main/java/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgmTreeWalker.g:116:3: '|'
							{
							match(input,26,FOLLOW_26_in_termPar350); 

								termResult+="|";
								
							}
							break;
						case 2 :
							// /home/eloi/Dropbox/IDEAWorkspace/Bgm2Java/src/main/java/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgmTreeWalker.g:119:3: '||'
							{
							match(input,27,FOLLOW_27_in_termPar357); 

								termResult+="||";
								regionCounter++;
								parents.clear();
								parents.add(new Region(regionCounter));
								nodes.add(new Region(regionCounter));
								
							}
							break;

					}

					pushFollow(FOLLOW_term_in_termPar365);
					t1=term();
					state._fsp--;

					termResult+=(t1!=null?((ConcreteBgmTreeWalker.term_return)t1).termResult:null);
					}
					break;

				default :
					break loop6;
				}
			}

			}

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return termResult;
	}
	// $ANTLR end "termPar"


	public static class term_return extends TreeRuleReturnScope {
		public String termResult;
		public PlaceList nodesResult;
	};


	// $ANTLR start "term"
	// /home/eloi/Dropbox/IDEAWorkspace/Bgm2Java/src/main/java/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgmTreeWalker.g:129:1: term returns [String termResult, PlaceList nodesResult] : (n= node ( '.' t0= term )? | '(' t1= termPar ')' | '$' ID | 'nil' );
	public final ConcreteBgmTreeWalker.term_return term() throws RecognitionException {
		ConcreteBgmTreeWalker.term_return retval = new ConcreteBgmTreeWalker.term_return();
		retval.start = input.LT(1);

		CommonTree ID3=null;
		TreeRuleReturnScope n =null;
		TreeRuleReturnScope t0 =null;
		String t1 =null;


			retval.nodesResult = new PlaceList();

		try {
			// /home/eloi/Dropbox/IDEAWorkspace/Bgm2Java/src/main/java/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgmTreeWalker.g:133:3: (n= node ( '.' t0= term )? | '(' t1= termPar ')' | '$' ID | 'nil' )
			int alt8=4;
			switch ( input.LA(1) ) {
			case ID:
				{
				alt8=1;
				}
				break;
			case 15:
				{
				alt8=2;
				}
				break;
			case 8:
				{
				alt8=3;
				}
				break;
			case 25:
				{
				alt8=4;
				}
				break;
			default:
				NoViableAltException nvae =
					new NoViableAltException("", 8, 0, input);
				throw nvae;
			}
			switch (alt8) {
				case 1 :
					// /home/eloi/Dropbox/IDEAWorkspace/Bgm2Java/src/main/java/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgmTreeWalker.g:133:3: n= node ( '.' t0= term )?
					{
					pushFollow(FOLLOW_node_in_term388);
					n=node();
					state._fsp--;


						retval.termResult=(n!=null?((ConcreteBgmTreeWalker.node_return)n).result:null);
						BigraphNode nodeTmp = (n!=null?((ConcreteBgmTreeWalker.node_return)n).node:null);

					// /home/eloi/Dropbox/IDEAWorkspace/Bgm2Java/src/main/java/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgmTreeWalker.g:137:2: ( '.' t0= term )?
					int alt7=2;
					int LA7_0 = input.LA(1);
					if ( (LA7_0==19) ) {
						alt7=1;
					}
					switch (alt7) {
						case 1 :
							// /home/eloi/Dropbox/IDEAWorkspace/Bgm2Java/src/main/java/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgmTreeWalker.g:137:2: '.' t0= term
							{

								//parent = nodeTmp;
								parents.add(nodeTmp);

							match(input,19,FOLLOW_19_in_term396); 
							pushFollow(FOLLOW_term_in_term400);
							t0=term();
							state._fsp--;


								retval.termResult += '.' + (t0!=null?((ConcreteBgmTreeWalker.term_return)t0).termResult:null);
								//parent = new Region(regionCounter);
								if(parents.size()>1){ 
									parents.remove(parents.size()-1);
								}

							}
							break;

					}


					 retval.nodesResult.add(nodeTmp);
					 if(isModel) {nodes.add(nodeTmp);};
					 if(isRule && !nodesTmp.contains(nodeTmp)) {nodesTmp.add(nodeTmp);};

					}
					break;
				case 2 :
					// /home/eloi/Dropbox/IDEAWorkspace/Bgm2Java/src/main/java/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgmTreeWalker.g:152:3: '(' t1= termPar ')'
					{
					match(input,15,FOLLOW_15_in_term410); 
					pushFollow(FOLLOW_termPar_in_term414);
					t1=termPar();
					state._fsp--;

					match(input,16,FOLLOW_16_in_term416); 
					retval.termResult='(' + t1 + ')'; 
					}
					break;
				case 3 :
					// /home/eloi/Dropbox/IDEAWorkspace/Bgm2Java/src/main/java/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgmTreeWalker.g:153:3: '$' ID
					{
					match(input,8,FOLLOW_8_in_term423); 
					ID3=(CommonTree)match(input,ID,FOLLOW_ID_in_term424); 
					retval.termResult='$'+(ID3!=null?ID3.getText():null);
					          if(isModel) {nodes.add(new Hole(Integer.parseInt((ID3!=null?ID3.getText():null)),parents.get(parents.size()-1)));};

					}
					break;
				case 4 :
					// /home/eloi/Dropbox/IDEAWorkspace/Bgm2Java/src/main/java/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgmTreeWalker.g:156:3: 'nil'
					{
					match(input,25,FOLLOW_25_in_term431); 
					retval.termResult="nil";
					}
					break;

			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "term"


	public static class node_return extends TreeRuleReturnScope {
		public String result;
		public BigraphNode node;
	};


	// $ANTLR start "node"
	// /home/eloi/Dropbox/IDEAWorkspace/Bgm2Java/src/main/java/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgmTreeWalker.g:158:1: node returns [String result, BigraphNode node] : i0= ID '_' i1= ID ( '[' n0= nameOrDash ( ',' n1= nameOrDash )* ']' )? ;
	public final ConcreteBgmTreeWalker.node_return node() throws RecognitionException {
		ConcreteBgmTreeWalker.node_return retval = new ConcreteBgmTreeWalker.node_return();
		retval.start = input.LT(1);

		CommonTree i0=null;
		CommonTree i1=null;
		String n0 =null;
		String n1 =null;


		      retval.result="";
		      List<String> names = new ArrayList<String>();
		      String id;
		      String ctrId;

		try {
			// /home/eloi/Dropbox/IDEAWorkspace/Bgm2Java/src/main/java/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgmTreeWalker.g:165:2: (i0= ID '_' i1= ID ( '[' n0= nameOrDash ( ',' n1= nameOrDash )* ']' )? )
			// /home/eloi/Dropbox/IDEAWorkspace/Bgm2Java/src/main/java/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgmTreeWalker.g:166:2: i0= ID '_' i1= ID ( '[' n0= nameOrDash ( ',' n1= nameOrDash )* ']' )?
			{
			i0=(CommonTree)match(input,ID,FOLLOW_ID_in_node456); 
			retval.result+=(i0!=null?i0.getText():null); id=(i0!=null?i0.getText():null);
			match(input,24,FOLLOW_24_in_node464); 
			i1=(CommonTree)match(input,ID,FOLLOW_ID_in_node470); 
			retval.result+="_"+(i1!=null?i1.getText():null);ctrId=(i1!=null?i1.getText():null);
			// /home/eloi/Dropbox/IDEAWorkspace/Bgm2Java/src/main/java/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgmTreeWalker.g:169:10: ( '[' n0= nameOrDash ( ',' n1= nameOrDash )* ']' )?
			int alt10=2;
			int LA10_0 = input.LA(1);
			if ( (LA10_0==22) ) {
				alt10=1;
			}
			switch (alt10) {
				case 1 :
					// /home/eloi/Dropbox/IDEAWorkspace/Bgm2Java/src/main/java/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgmTreeWalker.g:169:11: '[' n0= nameOrDash ( ',' n1= nameOrDash )* ']'
					{
					match(input,22,FOLLOW_22_in_node484); 
					retval.result+='[';
					pushFollow(FOLLOW_nameOrDash_in_node499);
					n0=nameOrDash();
					state._fsp--;

					retval.result+=n0; names.add(n0);
					// /home/eloi/Dropbox/IDEAWorkspace/Bgm2Java/src/main/java/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgmTreeWalker.g:171:10: ( ',' n1= nameOrDash )*
					loop9:
					while (true) {
						int alt9=2;
						int LA9_0 = input.LA(1);
						if ( (LA9_0==17) ) {
							alt9=1;
						}

						switch (alt9) {
						case 1 :
							// /home/eloi/Dropbox/IDEAWorkspace/Bgm2Java/src/main/java/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgmTreeWalker.g:171:11: ',' n1= nameOrDash
							{
							match(input,17,FOLLOW_17_in_node514); 
							retval.result+=',';
							pushFollow(FOLLOW_nameOrDash_in_node529);
							n1=nameOrDash();
							state._fsp--;

							retval.result+=n1;names.add(n1);
							}
							break;

						default :
							break loop9;
						}
					}

					match(input,23,FOLLOW_23_in_node539); 
					retval.result+=']';
					}
					break;

			}

			BigraphNode n = new BigraphNode(id, ctrId, names, parents.get(parents.size()-1));
			 retval.node = n;
			}

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "node"



	// $ANTLR start "nameOrDash"
	// /home/eloi/Dropbox/IDEAWorkspace/Bgm2Java/src/main/java/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgmTreeWalker.g:178:1: nameOrDash returns [String result] : ( ID | '-' );
	public final String nameOrDash() throws RecognitionException {
		String result = null;


		CommonTree ID4=null;

		try {
			// /home/eloi/Dropbox/IDEAWorkspace/Bgm2Java/src/main/java/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgmTreeWalker.g:178:36: ( ID | '-' )
			int alt11=2;
			int LA11_0 = input.LA(1);
			if ( (LA11_0==ID) ) {
				alt11=1;
			}
			else if ( (LA11_0==18) ) {
				alt11=2;
			}

			else {
				NoViableAltException nvae =
					new NoViableAltException("", 11, 0, input);
				throw nvae;
			}

			switch (alt11) {
				case 1 :
					// /home/eloi/Dropbox/IDEAWorkspace/Bgm2Java/src/main/java/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgmTreeWalker.g:178:38: ID
					{
					ID4=(CommonTree)match(input,ID,FOLLOW_ID_in_nameOrDash560); 
					result = (ID4!=null?ID4.getText():null);
					}
					break;
				case 2 :
					// /home/eloi/Dropbox/IDEAWorkspace/Bgm2Java/src/main/java/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgmTreeWalker.g:178:64: '-'
					{
					match(input,18,FOLLOW_18_in_nameOrDash566); 
					result = "-";
					}
					break;

			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return result;
	}
	// $ANTLR end "nameOrDash"

	// Delegated rules



	public static final BitSet FOLLOW_signature_in_program70 = new BitSet(new long[]{0x000000000200D120L});
	public static final BitSet FOLLOW_names_in_program78 = new BitSet(new long[]{0x000000000200C120L});
	public static final BitSet FOLLOW_rules_in_program86 = new BitSet(new long[]{0x0000000002008120L});
	public static final BitSet FOLLOW_model_in_program94 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_control_in_signature121 = new BitSet(new long[]{0x0000000000002602L});
	public static final BitSet FOLLOW_name_in_names151 = new BitSet(new long[]{0x0000000000001002L});
	public static final BitSet FOLLOW_rule_in_rules181 = new BitSet(new long[]{0x0000000000004002L});
	public static final BitSet FOLLOW_9_in_control202 = new BitSet(new long[]{0x0000000000000004L});
	public static final BitSet FOLLOW_ID_in_control206 = new BitSet(new long[]{0x0000000000000020L});
	public static final BitSet FOLLOW_ID_in_control210 = new BitSet(new long[]{0x0000000000000020L});
	public static final BitSet FOLLOW_ID_in_control214 = new BitSet(new long[]{0x0000000000000008L});
	public static final BitSet FOLLOW_13_in_control223 = new BitSet(new long[]{0x0000000000000004L});
	public static final BitSet FOLLOW_ID_in_control227 = new BitSet(new long[]{0x0000000000000020L});
	public static final BitSet FOLLOW_ID_in_control231 = new BitSet(new long[]{0x0000000000000020L});
	public static final BitSet FOLLOW_ID_in_control235 = new BitSet(new long[]{0x0000000000000008L});
	public static final BitSet FOLLOW_10_in_control244 = new BitSet(new long[]{0x0000000000000004L});
	public static final BitSet FOLLOW_ID_in_control248 = new BitSet(new long[]{0x0000000000000020L});
	public static final BitSet FOLLOW_ID_in_control252 = new BitSet(new long[]{0x0000000000000020L});
	public static final BitSet FOLLOW_ID_in_control256 = new BitSet(new long[]{0x0000000000000008L});
	public static final BitSet FOLLOW_12_in_name272 = new BitSet(new long[]{0x0000000000000004L});
	public static final BitSet FOLLOW_ID_in_name274 = new BitSet(new long[]{0x0000000000000008L});
	public static final BitSet FOLLOW_14_in_rule294 = new BitSet(new long[]{0x0000000000000004L});
	public static final BitSet FOLLOW_ID_in_rule296 = new BitSet(new long[]{0x0000000002008120L});
	public static final BitSet FOLLOW_termPar_in_rule300 = new BitSet(new long[]{0x0000000002008120L});
	public static final BitSet FOLLOW_termPar_in_rule304 = new BitSet(new long[]{0x0000000000000008L});
	public static final BitSet FOLLOW_termPar_in_model325 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_term_in_termPar343 = new BitSet(new long[]{0x000000000C000002L});
	public static final BitSet FOLLOW_26_in_termPar350 = new BitSet(new long[]{0x0000000002008120L});
	public static final BitSet FOLLOW_27_in_termPar357 = new BitSet(new long[]{0x0000000002008120L});
	public static final BitSet FOLLOW_term_in_termPar365 = new BitSet(new long[]{0x000000000C000002L});
	public static final BitSet FOLLOW_node_in_term388 = new BitSet(new long[]{0x0000000000080002L});
	public static final BitSet FOLLOW_19_in_term396 = new BitSet(new long[]{0x0000000002008120L});
	public static final BitSet FOLLOW_term_in_term400 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_15_in_term410 = new BitSet(new long[]{0x0000000002008120L});
	public static final BitSet FOLLOW_termPar_in_term414 = new BitSet(new long[]{0x0000000000010000L});
	public static final BitSet FOLLOW_16_in_term416 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_8_in_term423 = new BitSet(new long[]{0x0000000000000020L});
	public static final BitSet FOLLOW_ID_in_term424 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_25_in_term431 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_ID_in_node456 = new BitSet(new long[]{0x0000000001000000L});
	public static final BitSet FOLLOW_24_in_node464 = new BitSet(new long[]{0x0000000000000020L});
	public static final BitSet FOLLOW_ID_in_node470 = new BitSet(new long[]{0x0000000000400002L});
	public static final BitSet FOLLOW_22_in_node484 = new BitSet(new long[]{0x0000000000040020L});
	public static final BitSet FOLLOW_nameOrDash_in_node499 = new BitSet(new long[]{0x0000000000820000L});
	public static final BitSet FOLLOW_17_in_node514 = new BitSet(new long[]{0x0000000000040020L});
	public static final BitSet FOLLOW_nameOrDash_in_node529 = new BitSet(new long[]{0x0000000000820000L});
	public static final BitSet FOLLOW_23_in_node539 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_ID_in_nameOrDash560 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_18_in_nameOrDash566 = new BitSet(new long[]{0x0000000000000002L});
}
