// $ANTLR 3.5 /Users/eloipereira/Dropbox/IDEAWorkspace/Bgm2Java/src/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgm.g 2014-03-25 17:35:30
package edu.berkeley.eloi.concreteBgm2Java;

import org.antlr.runtime.*;
import org.antlr.runtime.tree.CommonTree;
import org.antlr.runtime.tree.CommonTreeAdaptor;
import org.antlr.runtime.tree.TreeAdaptor;


@SuppressWarnings("all")
public class ConcreteBgmParser extends Parser {
    public static final String[] tokenNames = new String[]{
            "<invalid>", "<EOR>", "<DOWN>", "<UP>", "COMMENT", "ID", "WS", "' -> '",
            "'$'", "'%active'", "'%atomic'", "'%check'", "'%name'", "'%passive'",
            "'%rule'", "'('", "')'", "','", "'-'", "'.'", "':'", "';'", "'['", "']'",
            "'_'", "'nil'", "'|'", "'||'"
    };
    public static final int EOF = -1;
    public static final int T__7 = 7;
    public static final int T__8 = 8;
    public static final int T__9 = 9;
    public static final int T__10 = 10;
    public static final int T__11 = 11;
    public static final int T__12 = 12;
    public static final int T__13 = 13;
    public static final int T__14 = 14;
    public static final int T__15 = 15;
    public static final int T__16 = 16;
    public static final int T__17 = 17;
    public static final int T__18 = 18;
    public static final int T__19 = 19;
    public static final int T__20 = 20;
    public static final int T__21 = 21;
    public static final int T__22 = 22;
    public static final int T__23 = 23;
    public static final int T__24 = 24;
    public static final int T__25 = 25;
    public static final int T__26 = 26;
    public static final int T__27 = 27;
    public static final int COMMENT = 4;
    public static final int ID = 5;
    public static final int WS = 6;

    // delegates
    public Parser[] getDelegates() {
        return new Parser[]{};
    }

    // delegators


    public ConcreteBgmParser(TokenStream input) {
        this(input, new RecognizerSharedState());
    }

    public ConcreteBgmParser(TokenStream input, RecognizerSharedState state) {
        super(input, state);
    }

    protected TreeAdaptor adaptor = new CommonTreeAdaptor();

    public void setTreeAdaptor(TreeAdaptor adaptor) {
        this.adaptor = adaptor;
    }

    public TreeAdaptor getTreeAdaptor() {
        return adaptor;
    }

    @Override
    public String[] getTokenNames() {
        return ConcreteBgmParser.tokenNames;
    }

    @Override
    public String getGrammarFileName() {
        return "/Users/eloipereira/Dropbox/IDEAWorkspace/Bgm2Java/src/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgm.g";
    }


    public static class program_return extends ParserRuleReturnScope {
        CommonTree tree;

        @Override
        public CommonTree getTree() {
            return tree;
        }
    }

    ;


    // $ANTLR start "program"
    // /Users/eloipereira/Dropbox/IDEAWorkspace/Bgm2Java/src/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgm.g:10:1: program : signature names rules model ( '%check' !)? ( ';' !)? ( EOF !) ;
    public final ConcreteBgmParser.program_return program() throws RecognitionException {
        ConcreteBgmParser.program_return retval = new ConcreteBgmParser.program_return();
        retval.start = input.LT(1);

        CommonTree root_0 = null;

        Token string_literal5 = null;
        Token char_literal6 = null;
        Token EOF7 = null;
        ParserRuleReturnScope signature1 = null;
        ParserRuleReturnScope names2 = null;
        ParserRuleReturnScope rules3 = null;
        ParserRuleReturnScope model4 = null;

        CommonTree string_literal5_tree = null;
        CommonTree char_literal6_tree = null;
        CommonTree EOF7_tree = null;

        try {
            // /Users/eloipereira/Dropbox/IDEAWorkspace/Bgm2Java/src/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgm.g:10:9: ( signature names rules model ( '%check' !)? ( ';' !)? ( EOF !) )
            // /Users/eloipereira/Dropbox/IDEAWorkspace/Bgm2Java/src/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgm.g:10:11: signature names rules model ( '%check' !)? ( ';' !)? ( EOF !)
            {
                root_0 = (CommonTree) adaptor.nil();


                pushFollow(FOLLOW_signature_in_program53);
                signature1 = signature();
                state._fsp--;

                adaptor.addChild(root_0, signature1.getTree());

                pushFollow(FOLLOW_names_in_program55);
                names2 = names();
                state._fsp--;

                adaptor.addChild(root_0, names2.getTree());

                pushFollow(FOLLOW_rules_in_program57);
                rules3 = rules();
                state._fsp--;

                adaptor.addChild(root_0, rules3.getTree());

                pushFollow(FOLLOW_model_in_program59);
                model4 = model();
                state._fsp--;

                adaptor.addChild(root_0, model4.getTree());

                // /Users/eloipereira/Dropbox/IDEAWorkspace/Bgm2Java/src/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgm.g:10:39: ( '%check' !)?
                int alt1 = 2;
                int LA1_0 = input.LA(1);
                if ((LA1_0 == 11)) {
                    alt1 = 1;
                }
                switch (alt1) {
                    case 1:
                        // /Users/eloipereira/Dropbox/IDEAWorkspace/Bgm2Java/src/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgm.g:10:40: '%check' !
                    {
                        string_literal5 = (Token) match(input, 11, FOLLOW_11_in_program62);
                    }
                    break;

                }

                // /Users/eloipereira/Dropbox/IDEAWorkspace/Bgm2Java/src/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgm.g:10:52: ( ';' !)?
                int alt2 = 2;
                int LA2_0 = input.LA(1);
                if ((LA2_0 == 21)) {
                    alt2 = 1;
                }
                switch (alt2) {
                    case 1:
                        // /Users/eloipereira/Dropbox/IDEAWorkspace/Bgm2Java/src/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgm.g:10:53: ';' !
                    {
                        char_literal6 = (Token) match(input, 21, FOLLOW_21_in_program68);
                    }
                    break;

                }

                // /Users/eloipereira/Dropbox/IDEAWorkspace/Bgm2Java/src/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgm.g:10:60: ( EOF !)
                // /Users/eloipereira/Dropbox/IDEAWorkspace/Bgm2Java/src/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgm.g:10:61: EOF !
                {
                    EOF7 = (Token) match(input, EOF, FOLLOW_EOF_in_program74);
                }

            }

            retval.stop = input.LT(-1);

            retval.tree = (CommonTree) adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        } catch (RecognitionException re) {
            reportError(re);
            recover(input, re);
            retval.tree = (CommonTree) adaptor.errorNode(input, retval.start, input.LT(-1), re);
        } finally {
            // do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "program"


    public static class signature_return extends ParserRuleReturnScope {
        CommonTree tree;

        @Override
        public CommonTree getTree() {
            return tree;
        }
    }

    ;


    // $ANTLR start "signature"
    // /Users/eloipereira/Dropbox/IDEAWorkspace/Bgm2Java/src/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgm.g:12:1: signature : ( control )* ;
    public final ConcreteBgmParser.signature_return signature() throws RecognitionException {
        ConcreteBgmParser.signature_return retval = new ConcreteBgmParser.signature_return();
        retval.start = input.LT(1);

        CommonTree root_0 = null;

        ParserRuleReturnScope control8 = null;


        try {
            // /Users/eloipereira/Dropbox/IDEAWorkspace/Bgm2Java/src/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgm.g:12:11: ( ( control )* )
            // /Users/eloipereira/Dropbox/IDEAWorkspace/Bgm2Java/src/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgm.g:12:13: ( control )*
            {
                root_0 = (CommonTree) adaptor.nil();


                // /Users/eloipereira/Dropbox/IDEAWorkspace/Bgm2Java/src/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgm.g:12:13: ( control )*
                loop3:
                while (true) {
                    int alt3 = 2;
                    int LA3_0 = input.LA(1);
                    if (((LA3_0 >= 9 && LA3_0 <= 10) || LA3_0 == 13)) {
                        alt3 = 1;
                    }

                    switch (alt3) {
                        case 1:
                            // /Users/eloipereira/Dropbox/IDEAWorkspace/Bgm2Java/src/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgm.g:12:13: control
                        {
                            pushFollow(FOLLOW_control_in_signature84);
                            control8 = control();
                            state._fsp--;

                            adaptor.addChild(root_0, control8.getTree());

                        }
                        break;

                        default:
                            break loop3;
                    }
                }

            }

            retval.stop = input.LT(-1);

            retval.tree = (CommonTree) adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        } catch (RecognitionException re) {
            reportError(re);
            recover(input, re);
            retval.tree = (CommonTree) adaptor.errorNode(input, retval.start, input.LT(-1), re);
        } finally {
            // do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "signature"


    public static class names_return extends ParserRuleReturnScope {
        CommonTree tree;

        @Override
        public CommonTree getTree() {
            return tree;
        }
    }

    ;


    // $ANTLR start "names"
    // /Users/eloipereira/Dropbox/IDEAWorkspace/Bgm2Java/src/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgm.g:14:1: names : ( name )* ;
    public final ConcreteBgmParser.names_return names() throws RecognitionException {
        ConcreteBgmParser.names_return retval = new ConcreteBgmParser.names_return();
        retval.start = input.LT(1);

        CommonTree root_0 = null;

        ParserRuleReturnScope name9 = null;


        try {
            // /Users/eloipereira/Dropbox/IDEAWorkspace/Bgm2Java/src/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgm.g:14:8: ( ( name )* )
            // /Users/eloipereira/Dropbox/IDEAWorkspace/Bgm2Java/src/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgm.g:14:10: ( name )*
            {
                root_0 = (CommonTree) adaptor.nil();


                // /Users/eloipereira/Dropbox/IDEAWorkspace/Bgm2Java/src/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgm.g:14:10: ( name )*
                loop4:
                while (true) {
                    int alt4 = 2;
                    int LA4_0 = input.LA(1);
                    if ((LA4_0 == 12)) {
                        alt4 = 1;
                    }

                    switch (alt4) {
                        case 1:
                            // /Users/eloipereira/Dropbox/IDEAWorkspace/Bgm2Java/src/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgm.g:14:10: name
                        {
                            pushFollow(FOLLOW_name_in_names94);
                            name9 = name();
                            state._fsp--;

                            adaptor.addChild(root_0, name9.getTree());

                        }
                        break;

                        default:
                            break loop4;
                    }
                }

            }

            retval.stop = input.LT(-1);

            retval.tree = (CommonTree) adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        } catch (RecognitionException re) {
            reportError(re);
            recover(input, re);
            retval.tree = (CommonTree) adaptor.errorNode(input, retval.start, input.LT(-1), re);
        } finally {
            // do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "names"


    public static class rules_return extends ParserRuleReturnScope {
        CommonTree tree;

        @Override
        public CommonTree getTree() {
            return tree;
        }
    }

    ;


    // $ANTLR start "rules"
    // /Users/eloipereira/Dropbox/IDEAWorkspace/Bgm2Java/src/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgm.g:16:1: rules : ( rule )* ;
    public final ConcreteBgmParser.rules_return rules() throws RecognitionException {
        ConcreteBgmParser.rules_return retval = new ConcreteBgmParser.rules_return();
        retval.start = input.LT(1);

        CommonTree root_0 = null;

        ParserRuleReturnScope rule10 = null;


        try {
            // /Users/eloipereira/Dropbox/IDEAWorkspace/Bgm2Java/src/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgm.g:16:7: ( ( rule )* )
            // /Users/eloipereira/Dropbox/IDEAWorkspace/Bgm2Java/src/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgm.g:16:9: ( rule )*
            {
                root_0 = (CommonTree) adaptor.nil();


                // /Users/eloipereira/Dropbox/IDEAWorkspace/Bgm2Java/src/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgm.g:16:9: ( rule )*
                loop5:
                while (true) {
                    int alt5 = 2;
                    int LA5_0 = input.LA(1);
                    if ((LA5_0 == 14)) {
                        alt5 = 1;
                    }

                    switch (alt5) {
                        case 1:
                            // /Users/eloipereira/Dropbox/IDEAWorkspace/Bgm2Java/src/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgm.g:16:9: rule
                        {
                            pushFollow(FOLLOW_rule_in_rules103);
                            rule10 = rule();
                            state._fsp--;

                            adaptor.addChild(root_0, rule10.getTree());

                        }
                        break;

                        default:
                            break loop5;
                    }
                }

            }

            retval.stop = input.LT(-1);

            retval.tree = (CommonTree) adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        } catch (RecognitionException re) {
            reportError(re);
            recover(input, re);
            retval.tree = (CommonTree) adaptor.errorNode(input, retval.start, input.LT(-1), re);
        } finally {
            // do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "rules"


    public static class control_return extends ParserRuleReturnScope {
        CommonTree tree;

        @Override
        public CommonTree getTree() {
            return tree;
        }
    }

    ;


    // $ANTLR start "control"
    // /Users/eloipereira/Dropbox/IDEAWorkspace/Bgm2Java/src/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgm.g:18:1: control : ( ( '%active' ^| '%passive' ^| '%atomic' ^) ID '_' ! ID ':' ! ID ';' !) ;
    public final ConcreteBgmParser.control_return control() throws RecognitionException {
        ConcreteBgmParser.control_return retval = new ConcreteBgmParser.control_return();
        retval.start = input.LT(1);

        CommonTree root_0 = null;

        Token string_literal11 = null;
        Token string_literal12 = null;
        Token string_literal13 = null;
        Token ID14 = null;
        Token char_literal15 = null;
        Token ID16 = null;
        Token char_literal17 = null;
        Token ID18 = null;
        Token char_literal19 = null;

        CommonTree string_literal11_tree = null;
        CommonTree string_literal12_tree = null;
        CommonTree string_literal13_tree = null;
        CommonTree ID14_tree = null;
        CommonTree char_literal15_tree = null;
        CommonTree ID16_tree = null;
        CommonTree char_literal17_tree = null;
        CommonTree ID18_tree = null;
        CommonTree char_literal19_tree = null;

        try {
            // /Users/eloipereira/Dropbox/IDEAWorkspace/Bgm2Java/src/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgm.g:18:9: ( ( ( '%active' ^| '%passive' ^| '%atomic' ^) ID '_' ! ID ':' ! ID ';' !) )
            // /Users/eloipereira/Dropbox/IDEAWorkspace/Bgm2Java/src/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgm.g:18:11: ( ( '%active' ^| '%passive' ^| '%atomic' ^) ID '_' ! ID ':' ! ID ';' !)
            {
                root_0 = (CommonTree) adaptor.nil();


                // /Users/eloipereira/Dropbox/IDEAWorkspace/Bgm2Java/src/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgm.g:18:11: ( ( '%active' ^| '%passive' ^| '%atomic' ^) ID '_' ! ID ':' ! ID ';' !)
                // /Users/eloipereira/Dropbox/IDEAWorkspace/Bgm2Java/src/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgm.g:18:12: ( '%active' ^| '%passive' ^| '%atomic' ^) ID '_' ! ID ':' ! ID ';' !
                {
                    // /Users/eloipereira/Dropbox/IDEAWorkspace/Bgm2Java/src/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgm.g:18:12: ( '%active' ^| '%passive' ^| '%atomic' ^)
                    int alt6 = 3;
                    switch (input.LA(1)) {
                        case 9: {
                            alt6 = 1;
                        }
                        break;
                        case 13: {
                            alt6 = 2;
                        }
                        break;
                        case 10: {
                            alt6 = 3;
                        }
                        break;
                        default:
                            NoViableAltException nvae =
                                    new NoViableAltException("", 6, 0, input);
                            throw nvae;
                    }
                    switch (alt6) {
                        case 1:
                            // /Users/eloipereira/Dropbox/IDEAWorkspace/Bgm2Java/src/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgm.g:18:13: '%active' ^
                        {
                            string_literal11 = (Token) match(input, 9, FOLLOW_9_in_control114);
                            string_literal11_tree = (CommonTree) adaptor.create(string_literal11);
                            root_0 = (CommonTree) adaptor.becomeRoot(string_literal11_tree, root_0);

                        }
                        break;
                        case 2:
                            // /Users/eloipereira/Dropbox/IDEAWorkspace/Bgm2Java/src/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgm.g:18:26: '%passive' ^
                        {
                            string_literal12 = (Token) match(input, 13, FOLLOW_13_in_control119);
                            string_literal12_tree = (CommonTree) adaptor.create(string_literal12);
                            root_0 = (CommonTree) adaptor.becomeRoot(string_literal12_tree, root_0);

                        }
                        break;
                        case 3:
                            // /Users/eloipereira/Dropbox/IDEAWorkspace/Bgm2Java/src/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgm.g:18:40: '%atomic' ^
                        {
                            string_literal13 = (Token) match(input, 10, FOLLOW_10_in_control124);
                            string_literal13_tree = (CommonTree) adaptor.create(string_literal13);
                            root_0 = (CommonTree) adaptor.becomeRoot(string_literal13_tree, root_0);

                        }
                        break;

                    }

                    ID14 = (Token) match(input, ID, FOLLOW_ID_in_control128);
                    ID14_tree = (CommonTree) adaptor.create(ID14);
                    adaptor.addChild(root_0, ID14_tree);

                    char_literal15 = (Token) match(input, 24, FOLLOW_24_in_control130);
                    ID16 = (Token) match(input, ID, FOLLOW_ID_in_control133);
                    ID16_tree = (CommonTree) adaptor.create(ID16);
                    adaptor.addChild(root_0, ID16_tree);

                    char_literal17 = (Token) match(input, 20, FOLLOW_20_in_control135);
                    ID18 = (Token) match(input, ID, FOLLOW_ID_in_control138);
                    ID18_tree = (CommonTree) adaptor.create(ID18);
                    adaptor.addChild(root_0, ID18_tree);

                    char_literal19 = (Token) match(input, 21, FOLLOW_21_in_control140);
                }

            }

            retval.stop = input.LT(-1);

            retval.tree = (CommonTree) adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        } catch (RecognitionException re) {
            reportError(re);
            recover(input, re);
            retval.tree = (CommonTree) adaptor.errorNode(input, retval.start, input.LT(-1), re);
        } finally {
            // do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "control"


    public static class name_return extends ParserRuleReturnScope {
        CommonTree tree;

        @Override
        public CommonTree getTree() {
            return tree;
        }
    }

    ;


    // $ANTLR start "name"
    // /Users/eloipereira/Dropbox/IDEAWorkspace/Bgm2Java/src/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgm.g:20:1: name : ( '%name' ^ ID ';' !) ;
    public final ConcreteBgmParser.name_return name() throws RecognitionException {
        ConcreteBgmParser.name_return retval = new ConcreteBgmParser.name_return();
        retval.start = input.LT(1);

        CommonTree root_0 = null;

        Token string_literal20 = null;
        Token ID21 = null;
        Token char_literal22 = null;

        CommonTree string_literal20_tree = null;
        CommonTree ID21_tree = null;
        CommonTree char_literal22_tree = null;

        try {
            // /Users/eloipereira/Dropbox/IDEAWorkspace/Bgm2Java/src/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgm.g:20:7: ( ( '%name' ^ ID ';' !) )
            // /Users/eloipereira/Dropbox/IDEAWorkspace/Bgm2Java/src/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgm.g:20:9: ( '%name' ^ ID ';' !)
            {
                root_0 = (CommonTree) adaptor.nil();


                // /Users/eloipereira/Dropbox/IDEAWorkspace/Bgm2Java/src/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgm.g:20:9: ( '%name' ^ ID ';' !)
                // /Users/eloipereira/Dropbox/IDEAWorkspace/Bgm2Java/src/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgm.g:20:10: '%name' ^ ID ';' !
                {
                    string_literal20 = (Token) match(input, 12, FOLLOW_12_in_name152);
                    string_literal20_tree = (CommonTree) adaptor.create(string_literal20);
                    root_0 = (CommonTree) adaptor.becomeRoot(string_literal20_tree, root_0);

                    ID21 = (Token) match(input, ID, FOLLOW_ID_in_name155);
                    ID21_tree = (CommonTree) adaptor.create(ID21);
                    adaptor.addChild(root_0, ID21_tree);

                    char_literal22 = (Token) match(input, 21, FOLLOW_21_in_name157);
                }

            }

            retval.stop = input.LT(-1);

            retval.tree = (CommonTree) adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        } catch (RecognitionException re) {
            reportError(re);
            recover(input, re);
            retval.tree = (CommonTree) adaptor.errorNode(input, retval.start, input.LT(-1), re);
        } finally {
            // do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "name"


    public static class rule_return extends ParserRuleReturnScope {
        CommonTree tree;

        @Override
        public CommonTree getTree() {
            return tree;
        }
    }

    ;


    // $ANTLR start "rule"
    // /Users/eloipereira/Dropbox/IDEAWorkspace/Bgm2Java/src/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgm.g:22:1: rule : ( '%rule' ^ ID termPar ' -> ' ! termPar ';' !) ;
    public final ConcreteBgmParser.rule_return rule() throws RecognitionException {
        ConcreteBgmParser.rule_return retval = new ConcreteBgmParser.rule_return();
        retval.start = input.LT(1);

        CommonTree root_0 = null;

        Token string_literal23 = null;
        Token ID24 = null;
        Token string_literal26 = null;
        Token char_literal28 = null;
        ParserRuleReturnScope termPar25 = null;
        ParserRuleReturnScope termPar27 = null;

        CommonTree string_literal23_tree = null;
        CommonTree ID24_tree = null;
        CommonTree string_literal26_tree = null;
        CommonTree char_literal28_tree = null;

        try {
            // /Users/eloipereira/Dropbox/IDEAWorkspace/Bgm2Java/src/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgm.g:22:7: ( ( '%rule' ^ ID termPar ' -> ' ! termPar ';' !) )
            // /Users/eloipereira/Dropbox/IDEAWorkspace/Bgm2Java/src/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgm.g:22:9: ( '%rule' ^ ID termPar ' -> ' ! termPar ';' !)
            {
                root_0 = (CommonTree) adaptor.nil();


                // /Users/eloipereira/Dropbox/IDEAWorkspace/Bgm2Java/src/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgm.g:22:9: ( '%rule' ^ ID termPar ' -> ' ! termPar ';' !)
                // /Users/eloipereira/Dropbox/IDEAWorkspace/Bgm2Java/src/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgm.g:22:10: '%rule' ^ ID termPar ' -> ' ! termPar ';' !
                {
                    string_literal23 = (Token) match(input, 14, FOLLOW_14_in_rule169);
                    string_literal23_tree = (CommonTree) adaptor.create(string_literal23);
                    root_0 = (CommonTree) adaptor.becomeRoot(string_literal23_tree, root_0);

                    ID24 = (Token) match(input, ID, FOLLOW_ID_in_rule172);
                    ID24_tree = (CommonTree) adaptor.create(ID24);
                    adaptor.addChild(root_0, ID24_tree);

                    pushFollow(FOLLOW_termPar_in_rule174);
                    termPar25 = termPar();
                    state._fsp--;

                    adaptor.addChild(root_0, termPar25.getTree());

                    string_literal26 = (Token) match(input, 7, FOLLOW_7_in_rule176);
                    pushFollow(FOLLOW_termPar_in_rule179);
                    termPar27 = termPar();
                    state._fsp--;

                    adaptor.addChild(root_0, termPar27.getTree());

                    char_literal28 = (Token) match(input, 21, FOLLOW_21_in_rule181);
                }

            }

            retval.stop = input.LT(-1);

            retval.tree = (CommonTree) adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        } catch (RecognitionException re) {
            reportError(re);
            recover(input, re);
            retval.tree = (CommonTree) adaptor.errorNode(input, retval.start, input.LT(-1), re);
        } finally {
            // do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "rule"


    public static class model_return extends ParserRuleReturnScope {
        CommonTree tree;

        @Override
        public CommonTree getTree() {
            return tree;
        }
    }

    ;


    // $ANTLR start "model"
    // /Users/eloipereira/Dropbox/IDEAWorkspace/Bgm2Java/src/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgm.g:24:1: model : termPar ';' !;
    public final ConcreteBgmParser.model_return model() throws RecognitionException {
        ConcreteBgmParser.model_return retval = new ConcreteBgmParser.model_return();
        retval.start = input.LT(1);

        CommonTree root_0 = null;

        Token char_literal30 = null;
        ParserRuleReturnScope termPar29 = null;

        CommonTree char_literal30_tree = null;

        try {
            // /Users/eloipereira/Dropbox/IDEAWorkspace/Bgm2Java/src/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgm.g:24:8: ( termPar ';' !)
            // /Users/eloipereira/Dropbox/IDEAWorkspace/Bgm2Java/src/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgm.g:24:11: termPar ';' !
            {
                root_0 = (CommonTree) adaptor.nil();


                pushFollow(FOLLOW_termPar_in_model193);
                termPar29 = termPar();
                state._fsp--;

                adaptor.addChild(root_0, termPar29.getTree());

                char_literal30 = (Token) match(input, 21, FOLLOW_21_in_model196);
            }

            retval.stop = input.LT(-1);

            retval.tree = (CommonTree) adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        } catch (RecognitionException re) {
            reportError(re);
            recover(input, re);
            retval.tree = (CommonTree) adaptor.errorNode(input, retval.start, input.LT(-1), re);
        } finally {
            // do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "model"


    public static class termPar_return extends ParserRuleReturnScope {
        CommonTree tree;

        @Override
        public CommonTree getTree() {
            return tree;
        }
    }

    ;


    // $ANTLR start "termPar"
    // /Users/eloipereira/Dropbox/IDEAWorkspace/Bgm2Java/src/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgm.g:26:1: termPar : term ( ( '|' | '||' ) term )* ;
    public final ConcreteBgmParser.termPar_return termPar() throws RecognitionException {
        ConcreteBgmParser.termPar_return retval = new ConcreteBgmParser.termPar_return();
        retval.start = input.LT(1);

        CommonTree root_0 = null;

        Token set32 = null;
        ParserRuleReturnScope term31 = null;
        ParserRuleReturnScope term33 = null;

        CommonTree set32_tree = null;

        try {
            // /Users/eloipereira/Dropbox/IDEAWorkspace/Bgm2Java/src/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgm.g:26:9: ( term ( ( '|' | '||' ) term )* )
            // /Users/eloipereira/Dropbox/IDEAWorkspace/Bgm2Java/src/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgm.g:26:11: term ( ( '|' | '||' ) term )*
            {
                root_0 = (CommonTree) adaptor.nil();


                pushFollow(FOLLOW_term_in_termPar205);
                term31 = term();
                state._fsp--;

                adaptor.addChild(root_0, term31.getTree());

                // /Users/eloipereira/Dropbox/IDEAWorkspace/Bgm2Java/src/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgm.g:26:16: ( ( '|' | '||' ) term )*
                loop7:
                while (true) {
                    int alt7 = 2;
                    int LA7_0 = input.LA(1);
                    if (((LA7_0 >= 26 && LA7_0 <= 27))) {
                        alt7 = 1;
                    }

                    switch (alt7) {
                        case 1:
                            // /Users/eloipereira/Dropbox/IDEAWorkspace/Bgm2Java/src/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgm.g:26:17: ( '|' | '||' ) term
                        {
                            set32 = input.LT(1);
                            if ((input.LA(1) >= 26 && input.LA(1) <= 27)) {
                                input.consume();
                                adaptor.addChild(root_0, (CommonTree) adaptor.create(set32));
                                state.errorRecovery = false;
                            } else {
                                MismatchedSetException mse = new MismatchedSetException(null, input);
                                throw mse;
                            }
                            pushFollow(FOLLOW_term_in_termPar216);
                            term33 = term();
                            state._fsp--;

                            adaptor.addChild(root_0, term33.getTree());

                        }
                        break;

                        default:
                            break loop7;
                    }
                }

            }

            retval.stop = input.LT(-1);

            retval.tree = (CommonTree) adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        } catch (RecognitionException re) {
            reportError(re);
            recover(input, re);
            retval.tree = (CommonTree) adaptor.errorNode(input, retval.start, input.LT(-1), re);
        } finally {
            // do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "termPar"


    public static class term_return extends ParserRuleReturnScope {
        CommonTree tree;

        @Override
        public CommonTree getTree() {
            return tree;
        }
    }

    ;


    // $ANTLR start "term"
    // /Users/eloipereira/Dropbox/IDEAWorkspace/Bgm2Java/src/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgm.g:28:1: term : ( node ( '.' term )? | '(' termPar ')' | '$' ID | 'nil' );
    public final ConcreteBgmParser.term_return term() throws RecognitionException {
        ConcreteBgmParser.term_return retval = new ConcreteBgmParser.term_return();
        retval.start = input.LT(1);

        CommonTree root_0 = null;

        Token char_literal35 = null;
        Token char_literal37 = null;
        Token char_literal39 = null;
        Token char_literal40 = null;
        Token ID41 = null;
        Token string_literal42 = null;
        ParserRuleReturnScope node34 = null;
        ParserRuleReturnScope term36 = null;
        ParserRuleReturnScope termPar38 = null;

        CommonTree char_literal35_tree = null;
        CommonTree char_literal37_tree = null;
        CommonTree char_literal39_tree = null;
        CommonTree char_literal40_tree = null;
        CommonTree ID41_tree = null;
        CommonTree string_literal42_tree = null;

        try {
            // /Users/eloipereira/Dropbox/IDEAWorkspace/Bgm2Java/src/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgm.g:28:7: ( node ( '.' term )? | '(' termPar ')' | '$' ID | 'nil' )
            int alt9 = 4;
            switch (input.LA(1)) {
                case ID: {
                    alt9 = 1;
                }
                break;
                case 15: {
                    alt9 = 2;
                }
                break;
                case 8: {
                    alt9 = 3;
                }
                break;
                case 25: {
                    alt9 = 4;
                }
                break;
                default:
                    NoViableAltException nvae =
                            new NoViableAltException("", 9, 0, input);
                    throw nvae;
            }
            switch (alt9) {
                case 1:
                    // /Users/eloipereira/Dropbox/IDEAWorkspace/Bgm2Java/src/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgm.g:28:10: node ( '.' term )?
                {
                    root_0 = (CommonTree) adaptor.nil();


                    pushFollow(FOLLOW_node_in_term228);
                    node34 = node();
                    state._fsp--;

                    adaptor.addChild(root_0, node34.getTree());

                    // /Users/eloipereira/Dropbox/IDEAWorkspace/Bgm2Java/src/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgm.g:28:15: ( '.' term )?
                    int alt8 = 2;
                    int LA8_0 = input.LA(1);
                    if ((LA8_0 == 19)) {
                        alt8 = 1;
                    }
                    switch (alt8) {
                        case 1:
                            // /Users/eloipereira/Dropbox/IDEAWorkspace/Bgm2Java/src/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgm.g:28:16: '.' term
                        {
                            char_literal35 = (Token) match(input, 19, FOLLOW_19_in_term231);
                            char_literal35_tree = (CommonTree) adaptor.create(char_literal35);
                            adaptor.addChild(root_0, char_literal35_tree);

                            pushFollow(FOLLOW_term_in_term233);
                            term36 = term();
                            state._fsp--;

                            adaptor.addChild(root_0, term36.getTree());

                        }
                        break;

                    }

                }
                break;
                case 2:
                    // /Users/eloipereira/Dropbox/IDEAWorkspace/Bgm2Java/src/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgm.g:28:30: '(' termPar ')'
                {
                    root_0 = (CommonTree) adaptor.nil();


                    char_literal37 = (Token) match(input, 15, FOLLOW_15_in_term240);
                    char_literal37_tree = (CommonTree) adaptor.create(char_literal37);
                    adaptor.addChild(root_0, char_literal37_tree);

                    pushFollow(FOLLOW_termPar_in_term242);
                    termPar38 = termPar();
                    state._fsp--;

                    adaptor.addChild(root_0, termPar38.getTree());

                    char_literal39 = (Token) match(input, 16, FOLLOW_16_in_term244);
                    char_literal39_tree = (CommonTree) adaptor.create(char_literal39);
                    adaptor.addChild(root_0, char_literal39_tree);

                }
                break;
                case 3:
                    // /Users/eloipereira/Dropbox/IDEAWorkspace/Bgm2Java/src/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgm.g:28:48: '$' ID
                {
                    root_0 = (CommonTree) adaptor.nil();


                    char_literal40 = (Token) match(input, 8, FOLLOW_8_in_term248);
                    char_literal40_tree = (CommonTree) adaptor.create(char_literal40);
                    adaptor.addChild(root_0, char_literal40_tree);

                    ID41 = (Token) match(input, ID, FOLLOW_ID_in_term249);
                    ID41_tree = (CommonTree) adaptor.create(ID41);
                    adaptor.addChild(root_0, ID41_tree);

                }
                break;
                case 4:
                    // /Users/eloipereira/Dropbox/IDEAWorkspace/Bgm2Java/src/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgm.g:28:56: 'nil'
                {
                    root_0 = (CommonTree) adaptor.nil();


                    string_literal42 = (Token) match(input, 25, FOLLOW_25_in_term253);
                    string_literal42_tree = (CommonTree) adaptor.create(string_literal42);
                    adaptor.addChild(root_0, string_literal42_tree);

                }
                break;

            }
            retval.stop = input.LT(-1);

            retval.tree = (CommonTree) adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        } catch (RecognitionException re) {
            reportError(re);
            recover(input, re);
            retval.tree = (CommonTree) adaptor.errorNode(input, retval.start, input.LT(-1), re);
        } finally {
            // do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "term"


    public static class node_return extends ParserRuleReturnScope {
        CommonTree tree;

        @Override
        public CommonTree getTree() {
            return tree;
        }
    }

    ;


    // $ANTLR start "node"
    // /Users/eloipereira/Dropbox/IDEAWorkspace/Bgm2Java/src/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgm.g:30:1: node : ID '_' ID ( '[' nameOrDash ( ',' nameOrDash )* ']' )? ;
    public final ConcreteBgmParser.node_return node() throws RecognitionException {
        ConcreteBgmParser.node_return retval = new ConcreteBgmParser.node_return();
        retval.start = input.LT(1);

        CommonTree root_0 = null;

        Token ID43 = null;
        Token char_literal44 = null;
        Token ID45 = null;
        Token char_literal46 = null;
        Token char_literal48 = null;
        Token char_literal50 = null;
        ParserRuleReturnScope nameOrDash47 = null;
        ParserRuleReturnScope nameOrDash49 = null;

        CommonTree ID43_tree = null;
        CommonTree char_literal44_tree = null;
        CommonTree ID45_tree = null;
        CommonTree char_literal46_tree = null;
        CommonTree char_literal48_tree = null;
        CommonTree char_literal50_tree = null;

        try {
            // /Users/eloipereira/Dropbox/IDEAWorkspace/Bgm2Java/src/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgm.g:30:7: ( ID '_' ID ( '[' nameOrDash ( ',' nameOrDash )* ']' )? )
            // /Users/eloipereira/Dropbox/IDEAWorkspace/Bgm2Java/src/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgm.g:30:9: ID '_' ID ( '[' nameOrDash ( ',' nameOrDash )* ']' )?
            {
                root_0 = (CommonTree) adaptor.nil();


                ID43 = (Token) match(input, ID, FOLLOW_ID_in_node262);
                ID43_tree = (CommonTree) adaptor.create(ID43);
                adaptor.addChild(root_0, ID43_tree);

                char_literal44 = (Token) match(input, 24, FOLLOW_24_in_node264);
                char_literal44_tree = (CommonTree) adaptor.create(char_literal44);
                adaptor.addChild(root_0, char_literal44_tree);

                ID45 = (Token) match(input, ID, FOLLOW_ID_in_node266);
                ID45_tree = (CommonTree) adaptor.create(ID45);
                adaptor.addChild(root_0, ID45_tree);

                // /Users/eloipereira/Dropbox/IDEAWorkspace/Bgm2Java/src/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgm.g:30:19: ( '[' nameOrDash ( ',' nameOrDash )* ']' )?
                int alt11 = 2;
                int LA11_0 = input.LA(1);
                if ((LA11_0 == 22)) {
                    alt11 = 1;
                }
                switch (alt11) {
                    case 1:
                        // /Users/eloipereira/Dropbox/IDEAWorkspace/Bgm2Java/src/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgm.g:30:20: '[' nameOrDash ( ',' nameOrDash )* ']'
                    {
                        char_literal46 = (Token) match(input, 22, FOLLOW_22_in_node269);
                        char_literal46_tree = (CommonTree) adaptor.create(char_literal46);
                        adaptor.addChild(root_0, char_literal46_tree);

                        pushFollow(FOLLOW_nameOrDash_in_node271);
                        nameOrDash47 = nameOrDash();
                        state._fsp--;

                        adaptor.addChild(root_0, nameOrDash47.getTree());

                        // /Users/eloipereira/Dropbox/IDEAWorkspace/Bgm2Java/src/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgm.g:30:35: ( ',' nameOrDash )*
                        loop10:
                        while (true) {
                            int alt10 = 2;
                            int LA10_0 = input.LA(1);
                            if ((LA10_0 == 17)) {
                                alt10 = 1;
                            }

                            switch (alt10) {
                                case 1:
                                    // /Users/eloipereira/Dropbox/IDEAWorkspace/Bgm2Java/src/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgm.g:30:36: ',' nameOrDash
                                {
                                    char_literal48 = (Token) match(input, 17, FOLLOW_17_in_node274);
                                    char_literal48_tree = (CommonTree) adaptor.create(char_literal48);
                                    adaptor.addChild(root_0, char_literal48_tree);

                                    pushFollow(FOLLOW_nameOrDash_in_node276);
                                    nameOrDash49 = nameOrDash();
                                    state._fsp--;

                                    adaptor.addChild(root_0, nameOrDash49.getTree());

                                }
                                break;

                                default:
                                    break loop10;
                            }
                        }

                        char_literal50 = (Token) match(input, 23, FOLLOW_23_in_node280);
                        char_literal50_tree = (CommonTree) adaptor.create(char_literal50);
                        adaptor.addChild(root_0, char_literal50_tree);

                    }
                    break;

                }

            }

            retval.stop = input.LT(-1);

            retval.tree = (CommonTree) adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        } catch (RecognitionException re) {
            reportError(re);
            recover(input, re);
            retval.tree = (CommonTree) adaptor.errorNode(input, retval.start, input.LT(-1), re);
        } finally {
            // do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "node"


    public static class nameOrDash_return extends ParserRuleReturnScope {
        CommonTree tree;

        @Override
        public CommonTree getTree() {
            return tree;
        }
    }

    ;


    // $ANTLR start "nameOrDash"
    // /Users/eloipereira/Dropbox/IDEAWorkspace/Bgm2Java/src/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgm.g:32:1: nameOrDash : ( ID | '-' );
    public final ConcreteBgmParser.nameOrDash_return nameOrDash() throws RecognitionException {
        ConcreteBgmParser.nameOrDash_return retval = new ConcreteBgmParser.nameOrDash_return();
        retval.start = input.LT(1);

        CommonTree root_0 = null;

        Token set51 = null;

        CommonTree set51_tree = null;

        try {
            // /Users/eloipereira/Dropbox/IDEAWorkspace/Bgm2Java/src/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgm.g:32:12: ( ID | '-' )
            // /Users/eloipereira/Dropbox/IDEAWorkspace/Bgm2Java/src/edu/berkeley/eloi/concreteBgm2Java/ConcreteBgm.g:
            {
                root_0 = (CommonTree) adaptor.nil();


                set51 = input.LT(1);
                if (input.LA(1) == ID || input.LA(1) == 18) {
                    input.consume();
                    adaptor.addChild(root_0, (CommonTree) adaptor.create(set51));
                    state.errorRecovery = false;
                } else {
                    MismatchedSetException mse = new MismatchedSetException(null, input);
                    throw mse;
                }
            }

            retval.stop = input.LT(-1);

            retval.tree = (CommonTree) adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        } catch (RecognitionException re) {
            reportError(re);
            recover(input, re);
            retval.tree = (CommonTree) adaptor.errorNode(input, retval.start, input.LT(-1), re);
        } finally {
            // do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "nameOrDash"

    // Delegated rules


    public static final BitSet FOLLOW_signature_in_program53 = new BitSet(new long[]{0x000000000200D120L});
    public static final BitSet FOLLOW_names_in_program55 = new BitSet(new long[]{0x000000000200C120L});
    public static final BitSet FOLLOW_rules_in_program57 = new BitSet(new long[]{0x0000000002008120L});
    public static final BitSet FOLLOW_model_in_program59 = new BitSet(new long[]{0x0000000000200800L});
    public static final BitSet FOLLOW_11_in_program62 = new BitSet(new long[]{0x0000000000200000L});
    public static final BitSet FOLLOW_21_in_program68 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_program74 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_control_in_signature84 = new BitSet(new long[]{0x0000000000002602L});
    public static final BitSet FOLLOW_name_in_names94 = new BitSet(new long[]{0x0000000000001002L});
    public static final BitSet FOLLOW_rule_in_rules103 = new BitSet(new long[]{0x0000000000004002L});
    public static final BitSet FOLLOW_9_in_control114 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_13_in_control119 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_10_in_control124 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_ID_in_control128 = new BitSet(new long[]{0x0000000001000000L});
    public static final BitSet FOLLOW_24_in_control130 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_ID_in_control133 = new BitSet(new long[]{0x0000000000100000L});
    public static final BitSet FOLLOW_20_in_control135 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_ID_in_control138 = new BitSet(new long[]{0x0000000000200000L});
    public static final BitSet FOLLOW_21_in_control140 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_12_in_name152 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_ID_in_name155 = new BitSet(new long[]{0x0000000000200000L});
    public static final BitSet FOLLOW_21_in_name157 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_14_in_rule169 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_ID_in_rule172 = new BitSet(new long[]{0x0000000002008120L});
    public static final BitSet FOLLOW_termPar_in_rule174 = new BitSet(new long[]{0x0000000000000080L});
    public static final BitSet FOLLOW_7_in_rule176 = new BitSet(new long[]{0x0000000002008120L});
    public static final BitSet FOLLOW_termPar_in_rule179 = new BitSet(new long[]{0x0000000000200000L});
    public static final BitSet FOLLOW_21_in_rule181 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_termPar_in_model193 = new BitSet(new long[]{0x0000000000200000L});
    public static final BitSet FOLLOW_21_in_model196 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_term_in_termPar205 = new BitSet(new long[]{0x000000000C000002L});
    public static final BitSet FOLLOW_set_in_termPar208 = new BitSet(new long[]{0x0000000002008120L});
    public static final BitSet FOLLOW_term_in_termPar216 = new BitSet(new long[]{0x000000000C000002L});
    public static final BitSet FOLLOW_node_in_term228 = new BitSet(new long[]{0x0000000000080002L});
    public static final BitSet FOLLOW_19_in_term231 = new BitSet(new long[]{0x0000000002008120L});
    public static final BitSet FOLLOW_term_in_term233 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_15_in_term240 = new BitSet(new long[]{0x0000000002008120L});
    public static final BitSet FOLLOW_termPar_in_term242 = new BitSet(new long[]{0x0000000000010000L});
    public static final BitSet FOLLOW_16_in_term244 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_8_in_term248 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_ID_in_term249 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_25_in_term253 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ID_in_node262 = new BitSet(new long[]{0x0000000001000000L});
    public static final BitSet FOLLOW_24_in_node264 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_ID_in_node266 = new BitSet(new long[]{0x0000000000400002L});
    public static final BitSet FOLLOW_22_in_node269 = new BitSet(new long[]{0x0000000000040020L});
    public static final BitSet FOLLOW_nameOrDash_in_node271 = new BitSet(new long[]{0x0000000000820000L});
    public static final BitSet FOLLOW_17_in_node274 = new BitSet(new long[]{0x0000000000040020L});
    public static final BitSet FOLLOW_nameOrDash_in_node276 = new BitSet(new long[]{0x0000000000820000L});
    public static final BitSet FOLLOW_23_in_node280 = new BitSet(new long[]{0x0000000000000002L});
}
