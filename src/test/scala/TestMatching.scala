/**
 * Created by eloipereira on 4/15/14.
 */

import edu.berkeley.eloi.bigraph._
import org.scalatest.FunSuite

class TestMatching extends FunSuite {
  test("Test matching 0"){
    val bigraph = new Bigraph("l1_Location.(u0_UAV[network]|u1_UAV[network]) | l0_Location;")
    val redex = new Bigraph("l1_Location.(u1_UAV[network] | $0);")
    assert(BigMCAPI.matching(bigraph,redex))
  }

  test("Test matching 1"){
    val bigraph = new Bigraph("l1_Location.(u0_UAV[network]|u1_UAV[network]) | l0_Location;")
    val redex = new Bigraph("l0_Location.(u1_UAV[network] | $0);")
    assert(!BigMCAPI.matching(bigraph,redex))
  }

  test("test equals"){
    val bigraph = new Bigraph("l1_Location.(u0_UAV[network]|u1_UAV[network]) | l0_Location;")
    val redex = new Bigraph("l1_Location.(u1_UAV[network] | $0);")
    assert(BigMCAPI.matching(bigraph,redex))
  }
}
