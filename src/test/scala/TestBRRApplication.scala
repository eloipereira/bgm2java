/**
 * Created by eloipereira on 4/15/14.
 */

import edu.berkeley.eloi.bigraph._
import java.util.Arrays._
import scala.collection.JavaConversions._
import org.scalatest.FunSuite
import scala.collection.mutable.ArrayBuffer

class TestBRRApplication extends FunSuite {
   test("Move vehicle BRR 0"){
     val preBigraph = new Bigraph("l1_Location.(u0_UAV[network]|u1_UAV[network]) | l0_Location;")
     val brs: BRS = new BRS(preBigraph, false)
     val expectedBigraph = new Bigraph("l1_Location.u0_UAV[network] | l0_Location.u1_UAV[network];")
     brs.applyRules(asList(new BRR("l1_Location.(u1_UAV[network] | $0) | l0_Location.$1 -> l1_Location.$0 | l0_Location.(u1_UAV[network] | $1)")))
     val bigraphPost = brs.getBigraph
     assert(bigraphPost == expectedBigraph)
   }
  test("Bigraph with different regions"){
    val bigraph = new Bigraph("l0_Location || l1_Location;")
    assert(bigraph.getRegions.contains(new Region(0)))
    assert(bigraph.getRegions.contains(new Region(1)))
  }
  test("Move vehicle BRR 1"){
       val preBigraph = new Bigraph("l3_Location.l1_Location.(u0_UAV[network]|u1_UAV[network]) | l0_Location | l2_Location;")
       val brs: BRS = new BRS(preBigraph,  false)
       val expectedBigraph = new Bigraph("l3_Location.l1_Location.u0_UAV[network] | l0_Location.u1_UAV[network] | l2_Location;")
       brs.applyRules(asList(new BRR("l1_Location.(u1_UAV[network] | $1) || l0_Location.$0 -> l1_Location.($1) || l0_Location.(u1_UAV[network] | $0)")))
       val bigraphPost = brs.getBigraph
       assert(bigraphPost == expectedBigraph)
     }
  test("Get node"){
    val ps: PlaceList = new PlaceList()
    val n = new BigraphNode("l0","Location", new ArrayBuffer[String](),new Region(0))
    ps.add(n)
    ps.add(new Region(0))
    val bigraph: Bigraph = new Bigraph(ps)
    assert(bigraph.getNode("l0") == n)
  }
  test("Get parent"){
      val ps: PlaceList = new PlaceList()
      val l0 = new BigraphNode("l0","Location", new ArrayBuffer[String](),new Region(0))
      val r0 = new BigraphNode("r0","Robot", new ArrayBuffer[String](),l0)
      ps.add(l0)
      ps.add(r0)
      val bigraph: Bigraph = new Bigraph(ps)
      assert(bigraph.getParentOf("r0") == l0)
    }
  test("Test matching 0"){
    val bigraph = new Bigraph("l1_Location.(u0_UAV[network]|u1_UAV[network]) | l0_Location;")
    val redex = new Bigraph("l1_Location.(u1_UAV[network] | $0);")
    assert(bigraph.matching(redex))
  }

  test("Test matching 1"){
    val bigraph = new Bigraph("l1_Location.(u0_UAV[network]|u1_UAV[network]) | l0_Location;")
    val redex = new Bigraph("l0_Location.(u1_UAV[network] | $0);")
    assert(!bigraph.matching(redex))
  }

  test("test equals"){
    val bigraph = new Bigraph("l1_Location.(u0_UAV[network]|u1_UAV[network]) | l0_Location;")
    val redex = new Bigraph("l1_Location.(u1_UAV[network] | $0);")
    assert(bigraph.equals(redex))
  }
}
