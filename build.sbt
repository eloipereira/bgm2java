
name := "Bgm2Java"

releaseSettings

// disable using the Scala version in output paths and artifacts
crossPaths := false

resolvers += DefaultMavenRepository

resolvers += "My bitbucket maven releases repo" at "https://bitbucket.org/eloipereira/maven-repo-releases/raw/master"

libraryDependencies += "org.antlr" % "antlr-complete" % "3.5.2"

libraryDependencies += "org.scalatest" % "scalatest_2.10" % "2.0" % "test"

libraryDependencies += "bigraphvisualizer" % "bigraphvisualizer" % "1.0"

libraryDependencies += "bigmc" % "bigmc" % "2.0"

libraryDependencies += "commons-logging" % "commons-logging" % "1.2"

libraryDependencies += "log4j" % "log4j" % "1.2.17"

sonarSettings

publishTo <<= version { (v: String) =>
  if (v.trim.endsWith("SNAPSHOT"))
    Some(Resolver.file("file",  new File( Path.userHome.absolutePath+"/.m2/repository/snapshots" )) )
  else
    Some(Resolver.file("file",  new File( Path.userHome.absolutePath+"/.m2/repository/releases" )) )
}

